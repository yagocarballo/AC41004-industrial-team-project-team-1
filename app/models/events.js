module.exports = function(sequelize , DataTypes){

    var Events = sequelize.define('Events', {
        id: {
            allowNull: false,
            type: DataTypes.INTEGER(),
            primaryKey:true,
            autoIncrement:true
        },
        eventType:{
            allowNull: false,
            type: DataTypes.STRING
        },
        pointsPerEvent:{
            allowNull: true,
            type: DataTypes.INTEGER()
        },
            title:{
            allowNull: false,
            type: DataTypes.STRING
        },
        pointId:{
            allowNull:false,
            type: DataTypes.INTEGER()
        },
        description:{
            allowNull: false,
            type: DataTypes.STRING
        },
        workshopHosts:{
            allowNull: true,
            type: DataTypes.STRING,
            default: ''
        },
        startTime:{
            allowNull: true,
            type:DataTypes.DATE()
        },
        endTime:{
            allowNull: true,
            type:DataTypes.DATE()
        } ,
        createdAt:{
            allowNull: false,
            type:DataTypes.DATE()
        },
        updatedAt:{
            allowNull: true,
            type:DataTypes.DATE()
        }


    } , {
        classMethods: {
            associate : function(models) {
                Events.hasMany(models.EventAttendance ,{foreignKey: 'eventId'})
            }
        }
    });
        return Events;
};