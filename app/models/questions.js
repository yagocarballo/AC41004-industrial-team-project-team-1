// Quiz model
module.exports = function (sequelize, DataTypes) {

  var Questions = sequelize.define('Questions', {
	id: {
		allowNull: false,
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true
	},
    question: {
		allowNull: false,
		type: DataTypes.STRING
	},
    alternatives: {
		allowNull: true,
		type: DataTypes.STRING,
		default: ''
	},
    answer: {
		allowNull: false,
		type: DataTypes.STRING,
		default: ''
	}
  }, {
    classMethods: {
		associate: function (models) {
			Questions.belongsTo(models.Quiz, { foreignKey: 'quizID' });
		}
	}
  });

  return Questions;
};



