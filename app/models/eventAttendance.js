module.exports = function (sequelize, DataTypes) {
    var EventAttendance = sequelize.define('EventAttendance',{

        confirmed : {
            allowNull : true,
            type: DataTypes.BOOLEAN
        },
        attendee : {
            allowNull : false,
            type : DataTypes.INTEGER,
            unique: 'myCompositeIndexName'
        },
        attendeeName : {
            allowNull : false,
            type: DataTypes.STRING
        },
        eventId : {
            allowNull : false,
            type : DataTypes.INTEGER,
            unique: 'myCompositeIndexName'
        }
    } ,{

        classMethods: {
            associate: function (models) {
                EventAttendance.belongsTo(models.User, {
                    foreignKey: 'attendee'
                });
                EventAttendance.belongsTo(models.Events, {
                    foreignKey: 'eventId'

                })
            }
        }
    });

    return EventAttendance;
};
