// Quiz model
module.exports = function (sequelize, DataTypes) {

  var Quiz = sequelize.define('Quiz', {
	id: {
		allowNull: false,
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true
	},

    title: {
		allowNull: false,
		type: DataTypes.STRING
	},
    description: {
		allowNull: true,
		type: DataTypes.STRING,
		default: ''
	},
    resource: {
		allowNull: false,
		type: DataTypes.STRING,
		default: ''
	},
	  passrate: {
		  allowNull: false,
		  type: DataTypes.INTEGER,
		  default: 8
	  }
  }, {
    classMethods: {
		associate: function (models) {
		}
	}
  });

  return Quiz;
};



