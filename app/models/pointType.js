// PointType model
module.exports = function (sequelize, DataTypes) {

    var PointType = sequelize.define('PointType', {
            name: {
                type: DataTypes.STRING,
                allowNull: false,
				unique: true
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    PointType.hasMany(models.PointRecord, { foreignKey: 'pointID', onUpdate: 'SET NULL', onDelete: 'SET NULL' });
                }
            }
        });
    return PointType;
};

