// Page model
module.exports = function (sequelize, DataTypes) {

  var Page = sequelize.define('Page', {
	url: {
		allowNull: false,
		type: DataTypes.STRING,
		unique: true
	},
    title: {
		allowNull: false,
		type: DataTypes.STRING
	},
    description: {
		allowNull: true,
		type: DataTypes.STRING,
		default: ''
	},
    text: {
		allowNull: false,
		type: DataTypes.STRING,
		default: ''
	},
    newsFeed: {
		allowNull: false,
		type: DataTypes.BOOLEAN,
		default: false
	},
    hasQuizes: {
		allowNull: false,
		type: DataTypes.BOOLEAN,
		default: false
	}
  }, {
    classMethods: {
      associate: function (models) {
		Page.hasMany(models.Article);
      }
    }
  });

  return Page;
};



