// Slider model
module.exports = function (sequelize, DataTypes) {

  var Slider = sequelize.define('Slider', {
	id: {
		allowNull: false,
		type: DataTypes.STRING,
		primaryKey: true
	},
    type: {
		allowNull: false,
		type: DataTypes.STRING
	},
    picture: {
		allowNull: true,
		type: DataTypes.STRING,
		default: ''
	},
    url: {
		allowNull: true,
		type: DataTypes.STRING,
		default: ''
	},
    content: {
		allowNull: true,
		type: DataTypes.STRING,
		default: ''
	}
  }, {
    classMethods: {
      associate: function (models) {
      }
    }
  });

  return Slider;
};
