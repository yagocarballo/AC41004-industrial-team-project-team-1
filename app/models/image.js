// Article model
module.exports = function (sequelize, DataTypes) {

  var DBImage = sequelize.define('Image', {
    name: {
		allowNull: false,
		type: DataTypes.STRING,
		unique: true
	},
    type: {
		allowNull: false,
		type: DataTypes.STRING,
		default: 'image/png'
	},
    data: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
      }
    }
  });

  return DBImage;
};
