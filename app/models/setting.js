// User model
module.exports = function (sequelize, DataTypes) {

  var Setting = sequelize.define('Setting', {
    key: {
		type: DataTypes.STRING,
		allowNull: false,
		primaryKey: true
	},
    value: {
		type: DataTypes.STRING,
		allowNull: true,
		default: null
	}
  }, {
    classMethods: {
      associate: function (models) {}
    }
  });

  return Setting;
};
