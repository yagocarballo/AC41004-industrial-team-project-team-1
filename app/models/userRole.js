// User model
module.exports = function(sequelize, DataTypes) {

	var UserRole = sequelize.define('UserRole', {
		name: {
			allowNull: false,
			type: DataTypes.STRING,
			primaryKey: true
		},
	    adminAccess: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			default: false
		},
	    manageUsers: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			default: false
		},
	    managePages: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			default: false
		},
	    manageEvents: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			default: false
		}
	}, {
		classMethods: {
			associate: function(models) {}
		}
	});

	return UserRole;
};
