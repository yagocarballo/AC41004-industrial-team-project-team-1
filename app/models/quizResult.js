// QuizResult model
module.exports = function (sequelize, DataTypes) {

    var QuizResult = sequelize.define('QuizResult', {
        username: {
            type: DataTypes.TEXT,
            allowNull: false,
            primaryKey: true
        },
        quizID: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },

        result: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        status:
        {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                QuizResult.hasMany(models.User, { foreignKey: 'username', constraints: true });
                QuizResult.hasMany(models.Quiz, { foreignKey: 'quizId', constraints: true });
            }
        }
    });

    return QuizResult;
};

