// User model
module.exports = function (sequelize, DataTypes) {

  var User = sequelize.define('User', {
    firstName: {
		type: DataTypes.STRING,
		allowNull: false
	},
    secondName: {
		type: DataTypes.STRING,
		allowNull: false
	},
	country: {
		type: DataTypes.STRING,
		allowNull: false
	},
	university: {
		type: DataTypes.STRING,
		allowNull: false
	},
	status: {
		type: DataTypes.STRING,
		allowNull: false
	},
	college: {
		type: DataTypes.STRING,
		allowNull: false
	},
	degree: {
		type: DataTypes.STRING,
		allowNull: false
	},
	year: {
		type: DataTypes.STRING,
		allowNull: false
	},
	matriculation: {
		type: DataTypes.STRING,
		allowNull: false
	},
	memberOfSociety: {
		type: DataTypes.BOOLEAN,
		allowNull: false,
		default: false
	},
	username: {
		type: DataTypes.STRING,
		allowNull: false,
		unique: true
	},
    password: {
		type: DataTypes.STRING,
		allowNull: false
	},
    accessToken: {
		type: DataTypes.STRING,
		unique: true
	},
	email: {
		type: DataTypes.STRING,
		allowNull: false,
		unique: true
	},
	emailVerified: {
		type: DataTypes.BOOLEAN,
		default: false
	},
	universityEmail: {
		type: DataTypes.STRING,
		allowNull: false,
		unique: true
	},
	universityEmailVerified: {
		type: DataTypes.BOOLEAN,
		default: false
	},
	accountActivated: {
		type: DataTypes.BOOLEAN,
		default: false
	},
	resetPassword: {
		type: DataTypes.STRING,
		unique: true,
		default: null
	},
	validateEmailToken: {
		type: DataTypes.STRING,
		unique: true,
		default: null
	},
	facebook: {
		type: DataTypes.STRING,
		unique: true,
		default: null
	}
  }, {
    classMethods: {
      associate: function (models) {
		  User.belongsTo(models.UserRole, { foreignKey: 'Role', onUpdate: 'SET NULL', onDelete: 'SET NULL' });
      }
    }
  });

  return User;
};

