// Article model
module.exports = function (sequelize, DataTypes) {

  var Article = sequelize.define('Article', {
	url: {
		allowNull: false,
		type: DataTypes.STRING,
		unique: true
	},
    title: {
		allowNull: false,
		type: DataTypes.STRING
	},
    text: {
		allowNull: false,
		type: DataTypes.STRING,
		default: ''
	},
    sticky: {
		allowNull: false,
		type: DataTypes.BOOLEAN,
		default: false
	}
  }, {
    classMethods: {
      associate: function (models) {
		Article.belongsTo(models.User, { foreignKey: 'createdBy' });
      }
    }
  });

  return Article;
};
