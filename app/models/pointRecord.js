// PointRecord model
module.exports = function (sequelize, DataTypes) {

    var PointRecord = sequelize.define('PointRecord', {
        pointID: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        noOfPoints: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
		
        pointSource: {
            type: DataTypes.STRING,
            allowNull: false
        },
    },
        {
        classMethods: {
            associate: function (models) {
                PointRecord.belongsTo(models.User, { foreignKey: 'userID', onUpdate: 'SET NULL', onDelete: 'SET NULL' });
            }
        }
    });

    return PointRecord;
};

