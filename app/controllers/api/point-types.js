var express = require('express'),
	router = express.Router(),
	db = require('../../models'),
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

module.exports = function (app) {
	//////////////////////// CRUD
	
	/**
	 * @function get all point types
	 * @description Gets a point types from the Database with a given ID
	 * @param id (String): The point types.
	 */
	app.get('/api/point-types', function(req, res) {
		db.PointType.findAll().then(function (pointTypes) {
			res.json(pointTypes);
		});
	});
	
	/**
	 * @function create a points type
	 * @description Creates a points type.
	 * @body (json) the points type object to create
	 */
	app.post('/api/point-type', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify users
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Creates the article
		var output = db.PointType.insertOrUpdate({
			name: req.body.name
		}).then(function(pointType) {
			// Returns a JSON with the created points type
			res.json(pointType);
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});
	
	/**
	 * @function update a points type
	 * @description Update a points type.
	 * @body (json) the points type object to update
	 */
	app.put('/api/point-type/:id', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify users
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		db.PointType.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (pointType) {
			// If the Point Type Exists
			if (pointType) {
				// Updates the point type
				pointType.name = (req.body.name || article.name);
		
				// Saves the article
				pointType.save().then(function() {
					res.json(pointType);
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Point Type not found.'
				});
			}
		});
	});
	
	/**
	 * @function delete points type
	 * @description Deletes a points type from the Database with a given ID
	 * @param id (String): The Points Type
	 */
	app.delete('/api/point-type/:id', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can points type
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Finds the article with ID
		db.PointType.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (pointType) {
			// If the points type was found
			if (pointType) {
				// Delete points type
				pointType.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Point Type with ID: ' + pointType.id + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Point type not found.'
				});
			}
		});
	});
};