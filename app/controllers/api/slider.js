var express = require('express'),
	router = express.Router(),
	db = require('../../models');

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get slider
	 * @description Gets an slider from the Database with a given ID
	 * @param id (String): The Slider ID
	 */
	app.get('/api/slider/:id', function(req, res) {
		db.Slider.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (slider) {
			res.json(slider);
		});
	});

	/**
	 * @function get slider
	 * @description Gets an slider from the Database with a given Type
	 * @param id (String): The Slider Type
	 */
	app.get('/api/slider/type/:type', function(req, res) {
		db.Slider.findAll({
			where: {
				type: req.params.type
			}
		}).then(function (slider) {
			res.json(slider);
		});
	});

	/**
	 * @function create slider
	 * @description Creates an slider.
	 * @body (json) the slider object to create
	 */
	app.post('/api/slider', function(req, res) {
		// Creates the slider
		var output = db.Slider.create({
			id: 'slider_' + Date.now(),
			type: req.body.type,
			picture: req.body.picture,
			url: req.body.url,
			content: req.body.content
		}).then(function(slider) {
			// Returns a JSON with the created slider
			res.json(slider);
		})
			//.catch(function(error) {
			//res.status(409);
			//res.json({
			//	error: {
			//		code: 409,
			//		message: ''+error
			//	}
			//});

		//});

	});

	/**
	 * @function update slider
	 * @description Updates an slider from the Database with a given ID
	 * @param id (String): The Slider ID
	 * @body (json) the slider object to update
	 */
	app.put('/api/slider/:id', function(req, res) {
		db.Slider.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (slider) {
			// If the Slider Exists
			if (slider) {
				// Updates the slider
				slider.id = (req.body.id || slider.id);
				slider.type = (req.body.type || slider.type);
				slider.picture = (req.body.picture || slider.picture);
				slider.url = (req.body.url || slider.url);
				slider.content = (req.body.content || slider.content);

				// Saves the slider
				slider.save().then(function() {
					res.json(slider);
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						message: 'Slider not found'
					}
				});
			}
		});
	});

	/**
	 * @function delete slider
	 * @description Deletes an slider from the Database with a given ID
	 * @param id (String): The Slider ID
	 */
	app.delete('/api/slider/:id', function(req, res) {
		// Finds the slider with ID
		db.Slider.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (slider) {
			// If the Slider was found
			if (slider) {
				// Delete Slider
				slider.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Slider with ID: ' + req.param.id + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						message: 'Slider not found'
					}
				});
			}
		});
	});

	//////////////////////// QUERY

	/**
	 * @function get all sliders
	 * @description Gets all sliders from the database.
	 */
	app.get('/api/slider', function (req, res, next) {
		// Finds all the sliders in the Database and fetches them
		db.Slider.findAll().then(function (sliders) {
			// Returns a JSON with the sliders
			res.json(sliders);;
		});
	});

};
	