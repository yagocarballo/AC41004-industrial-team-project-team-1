var express = require('express'),
  router = express.Router(),
  db = require('../../models');

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get question
	 * @description Gets a quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 ***/
	app.get('/api/question/:id', function(req, res) {
		db.Questions.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (question) {
			res.json(question);
		});
	});

	/**
	 * @function create quiz
	 * @description Creates a new quiz.
	 * @body (json) the quiz object to create
	 */
	app.post('/api/question', function(req, res) {
		// Creates the quiz
		var output = db.Questions.create({
			question: req.body.question,
			alternatives: req.body.alternatives,
			answer: req.body.answer,
			quizID: req.body.quizID
		}).then(function(question) {
			// Returns a JSON with the created quiz
			res.json(question);
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: {
					code: 409,
					message: ''+error
				}
			});
		});
	});

	/**
	 * @function update quiz
	 * @description Updates an quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 * @body (json) the quiz object to update
	 */
	app.put('/api/question/:id', function(req, res) {
		db.Questions	.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (question) {
			// If the question Exists
			if (question) {
				// Updates the question
				question.id = (req.body.id || question.id);
				question.question = (req.body.question || question.question);
				question.alternatives = (req.body.alternatives || question.alternatives);
				question.answer = (req.body.answer || question.answer);
				question.quizID = (req.body.quizID || question.quizID);

				// Saves the question
				question.save().then(function() {
					res.json(question);
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						message: 'question not found'
					}
				});
			}
		});
	});

	/**
	 * @function delete quiz
	 * @description Deletes an quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 */
	app.delete('/api/question/:id', function(req, res) {
		// Finds the quiz with ID
		db.Questions.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (question) {
			// If the Quiz was found
			if (question) {
				// Delete Quiz
				question.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Question with ID: ' + req.param.id + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					error: {

						code: 404,
						message: 'Question not found'
					}
				});
			}
		});
	});

	//////////////////////// QUERY

	/**
	 * @function get all questions of a quiz
	 * @description Gets all questions of a quiz from the database.
	 ***/
	app.get('/api/questions/:quizID', function (req, res, next) {
		// Finds all the quizzes in the Database and fetches them
		db.Questions.findAll({
			where: {
				quizID: req.params.quizID
			}
		}).then(function (questions) {
			// Returns a JSON with the quizzes
			res.json({
				questions: questions
			});
		});
	});



};
