var express = require('express'),
	router = express.Router(),
	db = require('../../models')
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get user role
	 * @description Gets an user role from the Database with a given ID
	 * @param name (String): The user role ID
	 */
	app.get('/api/user-role/:name', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify user roles
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		db.UserRole.findOne({
			where: {
				name: req.params.name
			}
		}).then(function (role) {
			res.json(role);
		});
	});

	/**
	 * @function create user role
	 * @description Creates an user role.
	 * @body (json) the user role object to create
	 */
	app.post('/api/user-role', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify user roles
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Creates the role
		var output = db.UserRole.create({
			name: (req.body.name || role.name),
		    adminAccess: (req.body.adminAccess === true),
		    manageUsers: (req.body.manageUsers === true),
		    managePages: (req.body.managePages === true),
		    manageEvents: (req.body.manageEvents === true)
			
		}).then(function(role) {
			// Returns a JSON with the created role
			res.json(role);
			
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});

	/**
	 * @function update user role
	 * @description Updates an user role from the Database with a given ID
	 * @param name (String): The user role ID
	 * @body (json) the user role object to update
	 */
	app.put('/api/user-role/:name', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify user roles
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		db.UserRole.findOne({
			where: {
				name: req.params.name
			}
		}).then(function (role) {
			// If the UserRole Exists
			if (role) {
				// Updates the role
				role.name = (req.body.name || role.name);
			    role.adminAccess = (req.body.adminAccess === true);
			    role.manageUsers = (req.body.manageUsers === true);
			    role.managePages = (req.body.managePages === true);
			    role.manageEvents = (req.body.manageEvents === true);
		
				// Saves the role
				role.save().then(function() {
					res.json(role);
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Role not found.'
				});
			}
		});
	});

	/**
	 * @function delete user role
	 * @description Deletes an user role from the Database with a given ID
	 * @param name (String): The user role ID
	 */
	app.delete('/api/user-role/:name', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify user roles
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Finds the role with ID
		db.UserRole.findOne({
			where: {
				name: req.params.name
			}
		}).then(function (role) {
			// If the Role was found
			if (role) {
				// Delete User Role
				role.destroy().then(function() {
					res.json({
						status: 201,
						message: 'User Role with Name: ' + role.name + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Role not found.'
				});
			}
		});
	});
	
	//////////////////////// QUERY

	/**
	 * @function get all user roles
	 * @description Gets all user roles from the database.
	 */
	app.get('/api/user-roles', passport.authenticate('bearer', {
		session: false
	}), function (req, res, next) {
		// Checks whether this user can modify user roles
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Finds all the user roles in the Database and fetches them
		db.UserRole.findAll({order: [['name']]}).then(function (roles) {
			// Returns a JSON with the user roles
			res.json(roles);
		});
	});
};
	