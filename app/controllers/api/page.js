var express = require('express'),
	router = express.Router(),
	db = require('../../models'),
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get page
	 * @description Gets an page from the Database with a given ID
	 * @param id (String): The Page ID
	 */
	app.get('/api/page/:url', function(req, res) {
		db.Page.findOne({
			where: {
				url: req.params.url
			}
		}).then(function (page) {
			res.json(page);
		});
	});

	/**
	 * @function create page
	 * @description Creates an page.
	 * @body (json) the page object to create
	 */
	app.post('/api/page', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		if (req.authInfo.role.managePages !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Creates the page
		var output = db.Page.create({
			url: (req.body.url || ( 'page_' + Date.now() )),
			title: req.body.title,
			description: req.body.description,
			text: req.body.text,
			newsFeed: (req.body.newsFeed === true),
			hasQuizes: (req.body.hasQuizes === true)
		}).then(function(page) {
			// Returns a JSON with the created page
			res.json(page);
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});

	/**
	 * @function update page
	 * @description Updates an page from the Database with a given ID
	 * @param id (String): The Page ID
	 * @body (json) the page object to update
	 */
	app.put('/api/page/:url', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		console.log("put triggered");
		if (req.authInfo.role.managePages !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		db.Page.findOne({
			where: {
				url: req.params.url
			}
		}).then(function (page) {
			// If the Page Exists
			if (page) {
				// Updates the page
				page.url = (req.body.url || page.url);
				page.title = (req.body.title || page.title);
				page.description = (req.body.description || page.description);
				page.text = (req.body.text || page.text);
				page.newsFeed = (req.body.newsFeed === true);
				page.hasQuizes = (req.body.hasQuizes === true);
		
				// Saves the page
				page.save().then(function() {
					res.json(page);
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Page not found.'
				});
			}
		});
	});

	/**
	 * @function delete page
	 * @description Deletes an page from the Database with a given ID
	 * @param id (String): The Page ID
	 */
	app.delete('/api/page/:url', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		if (req.authInfo.role.managePages !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Finds the page with ID
		db.Page.findOne({
			where: {
				url: req.params.url
			}
		}).then(function (page) {
			// If the Page was found
			if (page) {
				// Delete Page
				page.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Page with ID: ' + req.param.url + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Page not found.'
				});
			}
		});
	});

	//////////////////////// QUERY

	/**
	 * @function get all pages
	 * @description Gets all pages from the database.
	 */
	app.get('/api/pages', function (req, res, next) {
		// Finds all the pages in the Database and fetches them
		db.Page.findAll({
			order: [['updatedAt', 'DESC']]
		}).then(function (pages) {
			// Returns a JSON with the pages
			res.json(pages);
		});
	});
	
	/**
	 * @function get all pages
	 * @description Gets all pages from the database.
	 */
	app.get('/api/newsFeeds', function (req, res, next) {
		// Finds all the pages in the Database and fetches them
		db.Page.findAll({
			order: [['updatedAt', 'DESC']],
			where: {
				newsFeed: true
			}
		}).then(function (pages) {
			// Returns a JSON with the pages
			res.json(pages);
		});
	});
	
	/**
	 * @function get all articles for a page
	 * @description Gets all articles for a page from the database.
	 * @param id (String): The Page ID
	 */
	app.get('/api/page/:id/articles', function (req, res, next) {
		// Finds all the articles for a page in the Database and fetches them
		db.Article.findAll({
			order: [['sticky', 'DESC'], ['updatedAt', 'DESC']],
			where: {
				PageId: req.params.id
			}
		}).then(function (articles) {
			// Returns a JSON with the pages
			res.json(articles);
		});
	});
};
	