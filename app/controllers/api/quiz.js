var express = require('express'),
  router = express.Router(),
  db = require('../../models');

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get quiz
	 * @description Gets a quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 **/
	app.get('/api/quiz/:id', function(req, res) {
		db.Quiz.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (quiz) {
			res.json(quiz);
		});
	});

	/**
	 * @function create quiz
	 * @description Creates a new quiz.
	 * @body (json) the quiz object to create
	 */
	app.post('/api/quiz', function(req, res) {
		// Creates the quiz
		var output = db.Quiz.create({
			title: req.body.title,
			description: req.body.description,
			resource: req.body.resources,
			passrate: req.body.passrate
		}).then(function(quiz) {
			// Returns a JSON with the created quiz
			res.json(quiz);
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: {
					code: 409,
					message: ''+error

				}
			});
		});
	});

	/**
	 * @function update quiz
	 * @description quizIDUpdates an quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 * @body (json) the quiz object to update
	 */
	app.put('/api/quiz/:id', function(req, res) {
		db.Quiz.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (quiz) {
			// If the quiz Exists
			if (quiz) {
				// Updates the quiz
				quiz.id = (req.body.id || quiz.id);
				quiz.title = (req.body.title || quiz.title);
				quiz.description = (req.body.description || quiz.description);
				quiz.resource = (req.body.resources || quiz.resource);
				quiz.passrate =  (req.body.passrate || quiz.passrate);
				// Saves the quiz
				quiz.save().then(function() {
					res.json(quiz);
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						message: 'quiz not found'
					}
				});
			}
		});
	});

	/**
	 * @function delete quiz
	 * @description Deletes an quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 */
	app.delete('/api/quiz/:id', function(req, res) {
		// Finds the quiz with ID
		db.Quiz.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (quiz) {
			// If the Quiz was found
			if (quiz) {
				// Delete Quiz
				quiz.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Quiz with ID: ' + req.param.id + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					error: {

						code: 404,
						message: 'Quiz not found'
					}
				});
			}
		});
	});

	//////////////////////// QUERY

	/**
	 * @function get all quizzes
	 * @description Gets all quizzes from the database.
	 **/
	app.get('/api/quizzes', function (req, res, next) {
		// Finds all the quizzes in the Database and fetches them
		db.Quiz.findAll({
		}).then(function (quizzes) {
			// Returns a JSON with the quizzes
			res.json({
				title: 'Quizzes',
				quizzes: quizzes
			});
		});
	});

	/**
	 * @function get quizzes the user has taken
	 * @description Gets quizzes from the database that the user logged in has taken.
	 * @param username (String): The user username
	 **/
	app.get('/api/quizzes/:username', passport.authenticate('bearer', {
		session: false
	}),
	 function (req, res, next) {
		// Checks whether this user is accessing their quizzes
		if (req.authInfo.scope.username !== req.params.username) {
			res.status(403);
			res.json({
				error: {
					code: 403,
					name: 'Access Denied',
					message: 'Not enough permissions to access this feature.'
				}
			});
			// Ends Here
			return;
		}

		// Finds all taken quizzes by the user in the Database and fetches them
		db.sequelize.query("SELECT result, status,q.title, q.passrate FROM QuizResults as qr JOIN Quizzes as q on q.id = qr.quizID where username= '" + req.params.username + "'	;"
		).then(function (quizzesTaken) {
			// Returns a JSON with the quizzes
			res.json({
				title: 'QuizzesTaken',
				quizzesTaken: quizzesTaken[0]
			});
		});
	});

	app.get('/api/quiz/:id/user/:username', passport.authenticate('bearer', {
		session: false
	}),function (req, res, next) {
		// Checks whether the user has a right to acccess this page.
		if (req.authInfo.scope.username !== req.params.username) {
			res.status(403);
			res.json({
				error: {
					code: 403,
					name: 'Access Denied',
					message: 'Not enough permissions to access this feature.'
				}
			});
			// Ends Here
			return;
		}
		db.sequelize.query("SELECT q.id, q.title, q.description, q.resource, q.passrate, quest.count, qr.status"+
			" FROM Quizzes as q"+
			" left outer join (select quizID, count(quizID) as 'count' from Questions GROUP BY quizID) as quest on quest.quizID = q.id"+
			" left outer join (select status, quizID from QuizResults where username= '" + req.params.username + "')  as qr on qr.quizID = q.id where q.id = '" + req.params.id + "';").then(function (info) {
			// Returns a JSON with the quiz info
			res.json(info[0][0]);
		});
	});

};
