var express = require('express'),
    router = express.Router(),
    db = require('../../models');

module.exports = function (app) {


    //RETRIEVE ALL
    app.get('/api/events', function (req, res) {
        console.log("events retrieved maddyson");
        db.Events.findAll({}).then(function (events) {
            res.json({
                title: 'Events',
                events: events
            });
        });
    });

    //RETRIEVE BY ID
    app.get('/api/event/:id', function (req, res) {
        console.log("events retrieved by id");
        console.log("USER EPTA " + req.user);
        db.Events.findOne({
            where: {
                id: req.params.id

            },
            include: [
                {
                    model: db.EventAttendance, where: {eventId: req.params.id},
                    include: [{
                        model: db.User
                    }]
                    ,
                    required: false
                }]
        }).then(function (event) {
            res.json(event);
        }).catch(function (error) {
            res.status(409);
            res.json({
                error: {
                    code: 409,
                    message: '' + error
                }
            })
        });
    });

    //DELETE
    app.delete('/api/event/:id', function (req, res) {
        db.Events.findOne({
            where: {
                id: req.params.id
            }
        }).then(function (events) {
            if (events) {
                events.destroy().then(function () {
                    res.json({
                        status: 201,
                        message: 'Event with ID: ' + req.params.id + ' Destroyed.'
                    });
                });
            } else {
                res.status(404);
                res.json({
                    error: {
                        code: 404,
                        message: 'Event not found'
                    }
                });
            }
        });
    });

    app.post('/api/event/:id/register/:id', function (req, res) {
        console.log("Member attending !!!");
        db.EventAttendance.create({
            attendee: req.body.attendee,
            eventId: req.body.eventId,
            attendeeName: req.body.attendeeName
        }).then(function (attendance) {
            console.log("ATTENDANCE : " + attendance);
            res.json(attendance);
        }).catch(function (error) {
            console.log("Error : " + error);
            res.status(409);
            res.json({message: "You are already registered for this event"});
        });

    });

    app.put('/api/event/:id/register', function (req, res) {
        db.EventAttendance.findOne({
            where: {
                id: req.params.id
            }

        }).then(function (attendance) {
            if (attendance) {
                attandance.attendee = req.body.attendee;
                attandance.eventId = req.body.eventId;
                attandance.save().then(function () {
                    res.json(event);
                });
            } else {
                res.status(409);
                res.json({
                    error: {
                        code: 409,
                        name: 'Already registered',
                        message: 'You are already going to this event '
                    }
                })
            }
        })
    });
    //CREATE
    app.post('/api/event', function (req, res) {
        db.Events.create({
            eventType: req.body.eventType,
            pointsPerEvent: req.body.pointsPerEvent,
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            title: req.body.title,
            pointId : req.body.pointId,
            description: req.body.description,
            workshopHosts: req.body.workshopHosts,
            createdAt: Date.now()


        }).then(function (event) {
            res.json(event);
        }).catch(function (error) {
            console.log(error);
            res.status(409);
            res.json({
                error: {
                    code: 409,
                    message: '' + error
                }
            });
        });
    });

    //UPDATE
    app.put('/api/event/:id', function (req, res) {
        console.log("event updated KHOVAN");
        db.Events.findOne({
            where: {
                id: req.params.id
            }
        }).then(function (event) {
            if (event) {
                event.eventType = ( req.body.eventType || event.eventType);
                event.title = ( req.body.title || event.title);
                event.pointsPerEvent = ( req.body.pointsPerEvent || event.pointsPerEvent);
                event.startTime = ( req.body.startTime || event.startTime);
                event.endTime = ( req.body.endTime || event.endTime);
                event.description = ( req.body.description || event.description);
                event.workshopHosts = ( req.body.workshopHosts || event.workshopHosts);
                event.save().then(function () {
                    res.json(event);
                });
            } else {
                res.status(404);
                res.json({
                    code: 404,
                    name: 'Not Found',
                    message: 'Event not found.'
                });
            }
        });
    });

    app.put('/api/event/:eventId/member/:userId/confirm', function (req, res) {
        db.EventAttendance.findOne({
            where: {
                attendee: req.params.userId,
                eventId: req.params.eventId
            }
        }).then(function (eventAttendance) {
            if (eventAttendance) {
                eventAttendance.confirmed = req.body.confirmed;
                eventAttendance.save().then(function () {
                    res.json(eventAttendance);
                });
            }
            else {
                res.status(404);
                res.json({
                    code: 404,
                    name: 'Not Found',
                    message: 'Attendance entry not found'
                })
            }
        })
    });


    //RETRIEVE ALL THAT A USER ASSISTED
    app.get('/api/events/:username/assisted', function (req, res) {
        var query = "select event.title 'title', event.eventType 'type', strftime('%d.%m.%Y', event.startTime) 'date', event.pointsPerEvent 'points' from `Events` as event " +
            "left outer join `EventAttendances` as attendance on attendance.eventId = event.id AND attendance.confirmed = '1'" +
            "left outer join `Users` as user on user.id = attendance.attendee " +
            "where user.username = '" + req.params.username + "' order by event.startTime, event.pointsPerEvent;";

        db.sequelize.query(query).then(function (assistedEvents) {
            res.json(assistedEvents[0]);
        });
    });
};


