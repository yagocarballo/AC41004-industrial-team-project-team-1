var express	= require('express');
var router	= express.Router();
var db		= require('../../models');
var configDB= require('../../../config/email_conf.js');
var config	= require('../../../config/config.js');

var bcrypt 			= require('bcryptjs');
var SHA512 			= require("crypto-js/sha512");
var hat      		= require('hat');
var passport 		= require('passport');
var LocalStrategy	= require('passport-local').Strategy;
var BearerStrategy	= require('passport-http-bearer').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var nodemailer		= require('nodemailer');
var request = require('request');

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: configDB.email,
        pass: configDB.password
    }
});

var dbUserToCleanUser = function (user) {
	var safeUser = user.dataValues;
	delete safeUser.password;
	return safeUser;
};

module.exports = function(app) {
	//////////////////////// CRUD
	
	app.get('/api/recapcha/validate/:response', function (req, res, next) {
		var ip = req.headers['x-forwarded-for'] || 
				req.connection.remoteAddress || 
				req.socket.remoteAddress ||
				req.connection.socket.remoteAddress;

		var url = 'https://www.google.com/recaptcha/api/siteverify?secret=6LfsQg0TAAAAAMWpGmF--DLXQ4LbH-HM7ut2MuNu';
		url += '&response=' + req.params.response;
		url += '&remoteip=' + ip;
		
		request(url, function (error, response, body) {
		  if (!error && response.statusCode == 200) {
			  res.json(JSON.parse(body));
		  } else {
			res.status(403);
			res.json({
				success: false,
				code: 403,
				name: 'Not a Human',
				message: 'Can\'t verify if you\'re a human or a machine.'
			});
		  }
		})
	});

    /**
     * @function get user profile
     * @description Gets a user profile from the database.
     */
    app.get('/api/user/:username/profile', function (req, res, next) {
		var users = db.sequelize.query("select user.id, user.username, user.firstName, user.secondName, user.email, user.country, user.university, " +
				"user.status, user.college, user.degree, user.year, user.matriculation, user.memberOfSociety, user.createdAt, coalesce(record.points, 0) as points from users as user " +
				"left outer join (select coalesce(sum(noOfPoints), 0) as 'points', userId from PointRecords group by userID) as record on record.userID = user.id " +
				"where username = '" + req.params.username + "'").then(function (users) {
					if (users[0][0]) {
						db.sequelize.query("select type.id as 'pointID', coalesce(record.userID, " + users[0][0].id + ") as 'userID', " + 
						"type.name as 'pointType', coalesce(record.points, 0) as 'points' from PointTypes as type " + 
						"left outer join (select distinct record.id, record.pointID, sum(record.noOfPoints) as 'points' " + 
						", record.userID from PointRecords as record where record.userID = " + users[0][0].id + 
						" group by pointID) as record on record.pointID = type.id order by points desc;")
						.then(function (pointTypes) {
							users[0][0].pointSummary = pointTypes[0];
						    // Returns a JSON with the user profile
						    res.json(users[0][0]);
						});
					} else {
						res.status(404);
						res.json({
							code: 404,
							name: 'Not Found',
							message: 'User not found.'
						});
					}
			});
    });

	/**
	 * @function get user
	 * @description Gets a user from the database.
	 * @param id (String): The User ID
	 */
	app.get('/api/user/:username', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		db.User.findOne({
			where: {
				username: req.params.username
			}
		}).then(function(user) {
			if (user) {
				res.json(dbUserToCleanUser(user));
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'User not found.'
				});
			}
		}).
		catch (function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});

	/**
	 * @function create user
	 * @description Creates an user.
	 * @body (json) the user object to create
	 */
	app.post('/api/user', function(req, res) {
		var salt = bcrypt.genSaltSync(10);
		var hash = bcrypt.hashSync(req.body.password, salt);

		// Creates the user
		var output = db.User.create({
			firstName: 			req.body.firstName,
			secondName: 		req.body.secondName,
			country: 			req.body.country,
			university: 		req.body.university,
			status: 			req.body.status,
			college: 			req.body.college,
			degree: 			req.body.degree,
			year: 				req.body.year,
			matriculation: 		req.body.matriculation,
			memberOfSociety: 	req.body.memberOfSociety,
			username: 			req.body.username,
			password: 			hash,
			email: 				req.body.email,
			universityEmail: 	req.body.universityEmail,
			mobileNumber: 		req.body.mobileNumber,
			contactNumber: 		req.body.contactNumber,
			validateEmailToken: hat()

		}).then(function(user) {
			var server = req.protocol + '://' + req.get('Host');
            var mailOptions = {
                from: 'The Enterprise Gym <info@enterprisegym.com>', // sender address
                to: user.email, // list of receivers
                subject: 'The Enterprise Gym: Validate Email', // Subject line
                text: 'You\re account has been created, to validate your email, follow this link "'+server+'/user/email/validate/'+user.validateEmailToken+'".',
                html: 'You\re account has been created, to validate your email, follow this link "'+server+'/user/email/validate/'+user.validateEmailToken+'".',
            };
			
            transporter.sendMail(mailOptions, function(error, info){
				// Returns a JSON with the created article
				res.json(dbUserToCleanUser(user));
            });
		}).
		catch (function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});

	/**
	 * @function: update user
	 * @description: Updates a user from the Database with a given username
	 * @param username (String): the user username
	 * @body (json): the user object to update
	 */
	app.put('/api/user/:username', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can update this user
		if (req.authInfo.role.manageUsers === false && req.authInfo.scope.username !== req.params.username) {
			res.status(403);
			res.json({
				error: {
					code: 403,
					name: 'Access Denied',
					message: 'Not enough permissions to access this feature.'
				}
			});
			
			// Ends Here
			return; 
		}
		
		db.User.findOne({
			where: {
				username: req.params.username
			}
		}).then(function(user) {
			// If the user Exists
			if (user) {
				// Updates the user details
				user.firstName		 = (req.body.firstName || user.firstName)
				user.secondName		 = (req.body.secondName || user.secondName)
				user.country		 = (req.body.country || user.country);
				user.university		 = (req.body.university || user.university);
				user.status			 = (req.body.status || user.status);
				user.college		 = (req.body.college || user.college);
				user.degree			 = (req.body.degree || user.degree);
				user.year			 = (req.body.year || user.year);
				user.matriculation	 = (req.body.matriculation || user.matriculation);
				user.memberOfSociety = (req.body.memberOfSociety === true);
				user.email			 = (req.body.email || user.email);
				user.universityEmail = (req.body.universityEmail || user.universityEmail);
				
				if (req.authInfo.role.manageUsers === true) {
					user.Role	 = (req.body.Role || user.Role);
					user.accountActivated	 = (req.body.accountActivated === true);
				}

				// Saves the user
				user.save().then(function() {
					res.json(dbUserToCleanUser(user));
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						name: 'Not Found',
						message: 'User not found'
					}
				});
			}
		});
	});

	/**
	 * @function delete user
	 * @description Deletes a user from the Database with a given username
	 * @param username (String): The user username
	 */
	app.delete('/api/user/:username', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can delete users
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				error: {
					code: 403,
					name: 'Access Denied',
					message: 'Not enough permissions to access this feature.'
				}
			});
			
			// Ends Here
			return; 
		}
		
		// Finds the user with the username
		db.User.findOne({
			where: {
				username: req.params.username
			}
		}).then(function(user) {
			// If the user was found
			if (user) {
				// Delete user
				user.destroy().then(function() {
					res.json({
						status: 201,
						message: 'User with username: ' + user.username + ' destroyed :)'
					});
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						name: 'Not Found',
						message: 'User not found :()'
					}
				});
			}
		});
	});
	
	/**
	 * @function get all users
	 * @description Gets all users from the database.
	 */
	app.get('/api/users', function (req, res, next) {
		// Finds all the articles in the Database and fetches them
		db.User.findAll({order: [['updatedAt', 'DESC']]}).then(function (users) {
			var usersList = [];
			for (var i=0;i<users.length;i++) {
				usersList.push({
					id: 				users[i].id,
					firstName: 			users[i].firstName,
					secondName: 		users[i].secondName,
					country: 			users[i].country,
					university: 		users[i].university,
					status: 			users[i].status,
					college: 			users[i].college,
					degree: 			users[i].degree,
					year: 				users[i].year,
					matriculation: 		users[i].matriculation,
					memberOfSociety: 	users[i].memberOfSociety,
					username: 			users[i].username,
					email: 				users[i].email,
					accountActivated: 	users[i].accountActivated,
					Role: 				users[i].Role
				});
			}
			
			// Returns a JSON with the articles
			res.json(usersList);
		});
	});
	
	//////////////////////// Actions
	
	/**
	 * @function: approves user
	 * @description: Approves a user from the Database with a given username
	 * @param username (String): the user username
	 */
	app.put('/api/user/:username/approve', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can approve users
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				error: {
					code: 403,
					name: 'Access Denied',
					message: 'Not enough permissions to access this feature.'
				}
			});
			
			// Ends Here
			return; 
		}
		
		// Finds the user
		db.User.findOne({
			where: {
				username: req.params.username
			}
		}).then(function(user) {
			// If the user Exists
			if (user) {
				// Updates the user details
				user.accountActivated = true;

				// Saves the user
				user.save().then(function() {
					res.json({
						message: 'Account Activated!'
					});
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						name: 'Not Found',
						message: 'User not found'
					}
				});
			}
		});
	});

	//////////////////////// Login

	/**
	 * @function: User login
	 * @desctiption: Check user credentials
	 * @body: Credentials (object): username and password
	 */
	app.post('/api/user/login', function(req, res, next) {
		passport.authenticate('local', function(err, user, info) {
			if (err) {
				res.status(err.code);
				res.json(err);
			} else if (!user) {
				res.json({
					message: info.message
				});
			} else {
				if (user.Role) {
					db.UserRole.findOne({
						where: {
							name: user.Role
						}
					}).then(function(role) {
						user.dataValues.roleInfo = role;
						res.json(dbUserToCleanUser(user));
					}).catch(function () {
						user.dataValues.roleInfo = null;
						res.json(dbUserToCleanUser(user));
					});
				} else {
					user.dataValues.roleInfo = null;
					res.json(dbUserToCleanUser(user));
				}
			}
		})(req, res, next);
	});
	
	/**
	 * @function: User Forgot (1st Step)
	 * @desctiption: Sends an email to reset user's password
	 * @body: email (object): email of the user.
	 */
	app.post('/api/user/forgot', function(req, res, next) {
		db.User.findOne({
			where: {
				email: req.body.email
			}
		}).then(function(user) {
			// If the user Exists
			if (user) {
				user.resetPassword = hat();
				user.save().then(function() {
					var server = req.protocol + '://' + req.get('Host');
                    var mailOptions = {
                        from: 'The Enterprise Gym <info@enterprisegym.com>', // sender address
                        to: user.email, // list of receivers
                        subject: 'The Enterprise Gym: Reset Password', // Subject line
                        text: 'A password resset action has been triggered in your account. If this was intentional, follow this link "'+server+'/user/forgot/'+user.resetPassword+'" otherwise just ignore this email.',
                        html: 'A password resset action has been triggered in your account. If this was intentional, follow this link "'+server+'/user/forgot/'+user.resetPassword+'" otherwise just ignore this email.',
                    };


                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
							res.status(409);
							res.json({
								code: 409,
								name: 'Email Error',
								message : 'Error sending the reset password email.'
							});
                        }else{
							res.json({
								message : 'Reset password email sent.'
							});
                        }
                    });
				});
				
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'User not found'
				});
			}
		});
	});
	
	/**
	 * @function: User Forgot (2nd step)
	 * @desctiption: Reset user's password
	 * @param token (String): The reset password token
	 * @body: password (object): password 2 times
	 */
	app.post('/api/user/forgot/:token', function(req, res, next) {
		if (req.body.new_password !== req.body.new_password_2) {
			res.json({
				error: {
					code: 409,
					name: 'Invalid Password',
					message: 'Passwords do not match.'
				}
			});
		} else {
			// Gets the user with this reset account token
			db.User.findOne({
				where: {
					resetPassword: req.params.token
				}
			}).then(function(user) {
				// If the user Exists
				if (user) {
					// Creates a salt and hashes the password
					var salt = bcrypt.genSaltSync(10);
					var hash = bcrypt.hashSync(req.body.new_password, salt);
				
					// Updates the database with the new password
					user.resetPassword = null;
					user.password = hash;
					user.save().then(function() {
						// Returns the user (without the password)
						res.json(dbUserToCleanUser(user));
					});
				
				} else {
					res.status(404);
					res.json({
						error: {
							code: 404,
							name: 'Not Found',
							message: 'User not found'
						}
					});
				}
			});
		}
	});
	
	/**
	 * @function: User Email Validate (2nd step)
	 * @desctiption: Validates the user password
	 * @param token (String): The validate email token
	 */
	app.get('/api/user/email/validate/:token', function(req, res, next) {
		// Gets the user with this reset account token
		db.User.findOne({
			where: {
				validateEmailToken: req.params.token
			}
		}).then(function(user) {
			// If the user Exists
			if (user) {
				// Updates the database with the new password
				user.validateEmailToken = null;
				user.emailVerified = true;
				user.save().then(function() {
					// Returns the user (without the password)
					res.json(dbUserToCleanUser(user));
				});
			
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						name: 'Not Found',
						message: 'User not found'
					}
				});
			}
		});
	});
	
	app.get('/api/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

	app.get('/api/auth/facebook/callback', function (req, res, next) {
		passport.authenticate('facebook', function(err, user, info) {
			if (err) {
				res.redirect('/user/login/oauth?error='+err.code);
			} else if (!user) {
				res.redirect('/user/login/oauth?error=404');
			} else {
				if (user.Role) {
					db.UserRole.findOne({
						where: {
							name: user.Role
						}
					}).then(function(role) {
						var url = '/user/login/oauth?access_token='+user.accessToken;
						url += '&username='+user.username;
						url += '&role='+role.name;
						url += '&admin-access='+role.adminAccess;
						url += '&manage-pages='+role.managePages;
						url += '&manage-events='+role.manageEvents;
						url += '&manage-users='+role.manageUsers;
						res.redirect(url);
					}).catch(function () {
						res.redirect('/user/login/oauth?access_token='+user.accessToken+'&username='+user.username);
					});
				} else {
					res.redirect('/user/login/oauth?access_token='+user.accessToken+'&username='+user.username);
				}
			}
		})(req, res, next);
	});

	//////////////////////// Passport.js Stuff

	/**
	 * @function Access Token handler
	 * @desctiption This Passport.js strategy is called when an API call to a restricted area is called.
	 * @param token (String): The Token of the user.
	 * @param done (function): Token valid or invalid callback
	 */
	passport.use(new BearerStrategy(
		function(token, done) {
			// Gets the user with the specified access token from the database
			db.User.findOne({
				where: {
					accessToken: token
				}
			}).then(function(user) {
				// If the user wasn't found, reply with an invalid access
				if (!user) {
					return done(null, false);
					
				// Else return the user
				} else {
					// Gets the User Role
					done(null, user, {
						scope: user
					});
				}
			}).
			catch (function(error) {
				// If it cannot return the user with that token, give an error
				done(error);
			});
		}
	));

	/**
	 * @function Local Login handler
	 * @desctiption This Passport.js strategy is called when the user tries to login with one of the local users
	 * @param username (String): The username.
	 * @param password (String): The password (From the client should be Hashed with SHA512)
	 * @param done (function): Token valid or invalid callback
	 */
	passport.use(new LocalStrategy(
		function(username, password, done) {
			// Gets the user from the database
			db.User.findOne({
				where: {
					username: username
				}
			}).then(function(user) {
				// Checks if it found the user and if the user is activated (so it's autorized to login)
				if (user && user.accountActivated) {
					// Compares the Salted password with the password provided
					var isPasswordValid = bcrypt.compareSync(password, user.password);
					
					// If password is valid, Generate an access token and return the user's info
					if (isPasswordValid === true) {
						if (!user.accessToken) {
							// Generates the Token
							user.accessToken = hat();
							
							// Saves the access token into the database
							user.save().then(function() {
								// Gets the User Role
								done(null, user, {
									scope: user
								});
							});
						} else {
							done(null, user, {
								scope: user
							});
						}
						
					} else {
						done({
							code: 403,
							message: 'Access denied : Go Away!'
						});
					}
				} else {
					done({
						code: 404,
						message: 'User not found'
					});
				}
			}).
			catch (function(error) {
				done({
					error: error,
					code: 409,
					name: error.name,
					message: error.message
				});
			});
		}));
		
		passport.use(new FacebookStrategy({
			clientID: config.facebook.id,
			clientSecret: config.facebook.secret,
			callbackURL: config.facebook.callback,
			enableProof: false,
			profileFields: ['id', 'emails']
			
			}, function(accessToken, refreshToken, profile, done) {
				db.User.findOne({
					where: {
						email: profile.emails[0].value
					}
					
				}).then(function (user) {
					if (user) {
						if (!user.accessToken) {
							// Generates the Token
							user.accessToken = hat();
							
							// Saves the access token into the database
							user.save().then(function() {
								// Gets the User Role
								done(null, user, {
									scope: user
								});
							});
						} else {
							done(null, user, {
								scope: user
							});
						}
					} else {
						done({
							code: 404,
							message: 'User not found'
						});
					}

				}).catch(function (err) {
					done(err, profile, {
						accessToken: accessToken,
						refreshToken: refreshToken,
						message: '-->'
					});
				});
			}
		));

    /**
     * @function get all users
     * @description Gets all users from the database.
     */
    app.get('/api/leaderboard', function (req, res, next) {
		var users = db.sequelize.query("SELECT u.id, u.firstName, u.secondName, u.college, u.university, u.degree, u.year, " +
			"SUM(pr.noOfPoints) AS noOfPoints " +
			"FROM PointRecords pr	" +
			"LEFT JOIN Users u " +
			"ON pr.userID = u.id " +
			"GROUP BY u.id " +
			"ORDER BY noOfPoints DESC").then(function (users) {
			    // Returns a JSON with the users
			    res.json({
			        title: 'Leaderboard',
			        users: users[0]
			    });
			});
    });
};
