var express = require('express'),
	router = express.Router(),
	db = require('../../models')
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

var fs  = require('fs');	
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

module.exports = function(app) {
	app.post('/api/image/upload', upload.single('image'), function(req, res) {
		// Image as Base64
		var image = fs.readFileSync(req.file.path).toString("base64");
		var imageName = (req.file.originalname || req.file.filename);
		
		// Saves the Image into the Database
		db.Image.create({
			name: decodeURIComponent(imageName),
			type: req.file.mimetype,
			data: image
			
		}).then(function(img) {
			var server = req.protocol + '://' + req.get('Host');
			res.json({
				message: 'Image saved to the Database.',
				url: server + '/api/image/' + imageName
			});
			
		}).catch(function (error) {
			var latestError = error.errors[0];
			if (latestError.type === 'unique violation' && latestError.path === 'name') {
				var server = req.protocol + '://' + req.get('Host');
				res.json({
					message: 'Image already in the Database.',
					url: server + '/api/image/' + imageName
				});
				
			} else {
				res.status(409);
				res.json({
					error: error,
					code: 409,
					name: error.name,
					message: error.message
				});
			}
		});
	});
	
	app.get('/api/image/:name', function(req, res) {
		db.Image.findOne({
			where: {
				name: decodeURIComponent(req.params.name)
			}
		}).then(function (img) {
			if (img) {
				var tmpPath = __dirname + '/../../../uploads/' + img.name;
				fs.writeFileSync(tmpPath, img.data, "base64");
				res.sendFile(img.name, { root: __dirname + '/../../../uploads/' });
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Image not found.'
				});
			}
		});
	});
	
	app.get('/api/images', function(req, res) {
		// Finds all the images in the Database and fetches them
		db.Image.findAll({
			order: [['name']],
			where: {
				$or : [
					{ type: 'image/png' },
					{ type: 'image/jpg' },
					{ type: 'image/jpeg' },
					{ type: 'image/gif' }
				]
			}
		}).then(function (images) {
			// Returns a JSON with the articles
			var server = req.protocol + '://' + req.get('Host');
			var imageList = [];
			for (var i=0;i<images.length;i++) {
				imageList.push({
					title: images[i].name,
					value: server + '/api/image/' + images[i].name
				});
			}
			
			res.json(imageList);
		});
	});
};
