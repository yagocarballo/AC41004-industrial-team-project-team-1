var express = require('express'),
	router = express.Router(),
	db = require('../../models')
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

module.exports = function (app) {
	/**
	 * @function get setting
	 * @description Gets an setting from the Database with a given ID
	 * @param id (String): The Setting ID
	 */
	app.get('/api/setting/:key', function(req, res) {
		db.Setting.findOne({
			where: {
				key: req.params.key
			}
		}).then(function (setting) {
			res.json(setting);
		});
	});

	/**
	 * @function create setting
	 * @description Creates an setting.
	 * @body (json) the setting object to create
	 */
	app.post('/api/setting', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		if (req.authInfo.role.adminAccess !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Creates the setting
		var output = db.Setting.insertOrUpdate({
			key: req.body.key,
			value: req.body.value
		}).then(function(setting) {
			// Returns a JSON with the created setting
			res.json({
				message: 'Can\'t deny or disclose.'
			});
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});
};
	