var express = require('express'),
	router = express.Router(),
	db = require('../../models')
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get article
	 * @description Gets an article from the Database with a given ID
	 * @param id (String): The Article ID
	 */
	app.get('/api/article/:url', function(req, res) {
		db.Article.findOne({
			where: {
				url: req.params.url
			}
		}).then(function (article) {
			res.json(article);
		});
	});

	/**
	 * @function create article
	 * @description Creates an article.
	 * @body (json) the article object to create
	 */
	app.post('/api/article', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		if (req.authInfo.role.managePages !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Creates the article
		var output = db.Article.create({
			url: (req.body.url || ( 'article_' + Date.now() )),
			title: req.body.title,
			text: req.body.text,
			PageId: req.body.pageId,
			sticky: (req.body.sticky === true)
		}).then(function(article) {
			// Returns a JSON with the created article
			res.json(article);
		}).catch(function(error) {
			res.status(409);
			res.json({
				error: error,
				code: 409,
				name: error.name,
				message: error.message
			});
		});
	});

	/**
	 * @function update article
	 * @description Updates an article from the Database with a given ID
	 * @param id (String): The Article ID
	 * @body (json) the article object to update
	 */
	app.put('/api/article/:url', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		if (req.authInfo.role.managePages !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		db.Article.findOne({
			where: {
				url: req.params.url
			}
		}).then(function (article) {
			// If the Article Exists
			if (article) {
				// Updates the article
				article.url = (req.body.url || article.url);
				article.title = (req.body.title || article.title);
				article.text = (req.body.text || article.text);
				article.PageId = (req.body.pageId || article.pageId);
				article.sticky = (req.body.sticky === true);
		
				// Saves the article
				article.save().then(function() {
					res.json(article);
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Article not found.'
				});
			}
		});
	});

	/**
	 * @function delete article
	 * @description Deletes an article from the Database with a given ID
	 * @param id (String): The Article ID
	 */
	app.delete('/api/article/:url', passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Checks whether this user can modify pages
		if (req.authInfo.role.managePages !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});
			
			// Ends Here
			return; 
		}
		
		// Finds the article with ID
		db.Article.findOne({
			where: {
				url: req.params.url
			}
		}).then(function (article) {
			// If the Article was found
			if (article) {
				// Delete Article
				article.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Article with ID: ' + req.param.id + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					code: 404,
					name: 'Not Found',
					message: 'Article not found.'
				});
			}
		});
	});

	//////////////////////// QUERY

	/**
	 * @function get all articles
	 * @description Gets all articles from the database.
	 */
	app.get('/api/articles', function (req, res, next) {
		// // Finds all the articles in the Database and fetches them
		// db.Article.findAll({
		// 	order: [['sticky', 'DESC'], ['updatedAt', 'DESC']]
		// }).then(function (articles) {
		// 	// Returns a JSON with the articles
		// 	res.json({
		// 		title: 'Articles',
		// 		articles: articles
		// 	});
		// });
		
		db.sequelize.query("SELECT article.id, article.url, article.title, article.sticky, article.text, " + 
			"article.createdAt, article.updatedAt, article.createdBy, article.PageId, page.title 'pageName' " + 
			"FROM `Articles` as article LEFT OUTER JOIN `Pages` as page ON article.PageId = page.id " +
			"ORDER BY article.sticky, article.updatedAt;").then(function (articles) {
			    // Returns a JSON with the articles
			    res.json(articles[0]);
			});
		});
};
	