var express = require('express'),
	router = express.Router(),
	db = require('../../models'),
	passport = require('passport'),
	BearerStrategy = require('passport-http-bearer').Strategy;

module.exports = function (app) {
	//////////////////////// CRUD

	/**
	 * @function get point record
	 * @description Gets a point record from the Database with a given ID
	 * @param id (String): The point record.
	 */
	app.get('/api/point-record/:id', function(req, res) {
		db.PointRecord.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (pointRecord) {
			res.json(pointRecord);
		});
	});
	
	app.delete('/api/point-record/reset', passport.authenticate('bearer', {
		session: false
	}), function (req, res) {
		// Checks whether this user can modify users
		if (req.authInfo.role.manageUsers !== true) {
			res.status(403);
			res.json({
				code: 403,
				name: 'Access Denied',
				message: 'Not enough permissions to access this feature.'
			});

			// Ends Here
			return;
		}
		
		db.sequelize.query("delete from PointRecords;").then(function (result) {
			res.json({
				message: 'Point records reseted.'
			});
		});
	});
	
	app.delete('/api/point-record/user/:userID/pointSource/:pointSource' , function(req,res){
		db.PointRecord.findOne({
			where:{
				pointSource : req.params.pointSource,
				userID : req.params.userID
			}
		}).then(function(pointRecord){
			if(pointRecord){
				pointRecord.destroy().then(function(){
					res.json({
						status: 201,
						message: 'Point record with ID: ' + pointRecord.id + ' Destroyed.'
					});
				});
			}else{
				res.status(404);
				res.json({
					error: {
						code: 404,
						message: 'Point record not found'
					}
				});
			}
		})
	});


		/**
		 * @function add a points record
		 * @description Creates a points type.
		 * @body (json) the points type object to create
		 */
		app.post('/api/point-record', passport.authenticate('bearer', {
			session: false
		}), function (req, res) {
			// Checks whether this user can modify users
			if (req.authInfo.role.manageUsers !== true) {
				res.status(403);
				res.json({
					code: 403,
					name: 'Access Denied',
					message: 'Not enough permissions to access this feature.'
				});

				// Ends Here
				return;
			}

			// Creates the points record
			var output = db.PointRecord.create({
				pointID: req.body.pointID,
				noOfPoints: req.body.noOfPoints,
				pointSource: req.body.pointSource,
				userID: req.body.userID

			}).then(function (pointRecord) {
				// Returns a JSON with the created points record
				res.json(pointRecord);
			}).catch(function (error) {
				res.status(409);
				res.json({
					error: error,
					code: 409,
					name: error.name,
					message: error.message
				});
			});
		});

		/**
		 * @function add a quiz-points record
		 * @description Creates a quiz-points type.
		 * @body (json) the points type object to create
		 */
		app.post('/api/quizpoint-record', function (req, res) {
			// Checks whether this user can modify users


			// Creates the points re	cord
			var output = db.PointRecord.create({
				pointID: 2,
				noOfPoints: 7,
				pointSource: req.body.pointSource,
				userID: req.body.userID

			}).then(function (pointRecord) {
				// Returns a JSON with the created points record
				res.json(pointRecord);
			}).catch(function (error) {
				res.status(409);
				res.json({
					error: error,
					code: 409,
					name: error.name,
					message: error.message
				});
			});
		});
		/**
		 * @function get all point records
		 * @description Gets all the point records from the database.
		 */
		app.get('/api/point-records', function (req, res, next) {
			var query = "select record.id, record.pointID, record.noOfPoints, record.pointSource, record.createdAt, " +
				"record.userID, type.name 'pointType', user.username, user.email, user.matriculation  from `PointRecords` as record " +
				"left outer join `PointTypes` as type on record.pointID = type.id " +
				"left outer join `Users` as user on record.userID = user.id ";

			if (req.query.username) {
				query += "where user.username = '" + req.query.username + "'";
			}

			if (req.query.from && req.query.to) {
				query += (req.query.username ? ' and ' : ' where ');
				query += " record.createdAt between '" + req.query.from + "' and '" + req.query.to + "'";
			}

			var pointRecords = db.sequelize.query(query).then(function (pointRecords) {
				// Returns a JSON with the point records
				res.json(pointRecords[0]);
			});
		});
	};
