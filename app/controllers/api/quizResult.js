var express = require('express'),
  router = express.Router(),
  db = require('../../models');

module.exports = function (app) {


	/**
	 * @function get all entries for a user
	 * @description Gets all the the quizResults of a user
	 * @param username (String) the username of the user
	 **/
	app.get('/api/quizResult/:username', function(req, res) {
		db.QuizResult.findAll({
			where: {
				username: req.params.username
			}
		}).then(function (quizResult) {
			res.json(quizResult);
		});
	});

	/**
	 * @function create quiz
	 * @description Creates a new quiz.
	 * @body (json) the quiz object to create
	 **/
	app.post('/api/quizResult',passport.authenticate('bearer', {
		session: false
	}), function(req, res) {
		// Creates the quiz
		var output = db.QuizResult.create({
			username: req.body.username,
			quizID: req.body.quizID,
			result: req.body.result,
			status: req.body.status
		}).then(function(quizResult) {
			// Returns a JSON with the created quiz
			res.json(quizResult);
		}).catch(function(error) {
			console.log(error);
			res.status(409);
			res.json({
				error: {
					code: 409,
					message: ''+error

				}
			});
		});
	});

	/**
	 * @function update quiz
	 * @description Updates an quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 * @body (json) the quiz object to update
	 *
	app.put('/api/quiz/:id', function(req, res) {
		db.Quiz.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (quiz) {
			// If the quiz Exists
			if (quiz) {
				// Updates the quiz
				quiz.id = (req.body.id || quiz.id);
				quiz.title = (req.body.title || quiz.title);
				quiz.description = (req.body.description || quiz.description);
				quiz.resource = (req.body.resources || quiz.resource);
				quiz.passrate =  (req.body.passrate || quiz.passrate);
				// Saves the quiz
				quiz.save().then(function() {
					res.json(quiz);
				});
			} else {
				res.status(404);
				res.json({
					error: {
						code: 404,
						message: 'quiz not found'
					}
				});
			}
		});
	});
*/
	/**
	 * @function delete quiz
	 * @description Deletes an quiz from the Database with a given ID
	 * @param id (String): The quiz ID
	 *
	app.delete('/api/quiz/:id', function(req, res) {
		// Finds the quiz with ID
		db.Quiz.findOne({
			where: {
				id: req.params.id
			}
		}).then(function (quiz) {
			// If the Quiz was found
			if (quiz) {
				// Delete Quiz
				quiz.destroy().then(function() {
					res.json({
						status: 201,
						message: 'Quiz with ID: ' + req.param.id + ' Destroyed.'
					});
				});
			} else {
				res.status(404);
				res.json({
					error: {

						code: 404,
						message: 'Quiz not found'
					}
				});
			}
		});
	});
*/
	//////////////////////// QUERY

	/**
	 * @function get all quiz Results
	 * @description Gets all quizzes from the database.
	 **/
	app.get('/api/quizResult', function (req, res, next) {
		// Finds all the quizzes in the Database and fetches them
		db.QuizResult.findAll({
		}).then(function (quizResult) {
			// Returns a JSON with the quizzes
			res.json(quizResult);
		});
	});




};
