var express = require('express'),
  router = express.Router(),
  db = require('../models');

module.exports = function (app) {
	/**
	 * @function gets info about the page
	 */
	app.get('/api', function(req, res) {
		res.json({
			version: '0.0.1',
			authors: [
				'Barker, Madeleine',
				'Bennington, Peter',
				'Carballo, Yago',
				'Denev, Plamen',
				'Vasiljevs, Andrejs',
				'Tengroth, Wiktor Johan Henrik',
				'Chang, Kang'
			]
		});
	});
};
