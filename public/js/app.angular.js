(function () {
    // Run function
    var _app_run = function ($rootScope, $state, $cookies) {
        // Checks between view transitions
        $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;

            // Checks if the user is logged in
            if (toState.restricted) {
                var access_token = $cookies.get('access-token', null);
                var roleName = $cookies.get('role-name', null);
                var hasAccess = JSON.parse($cookies.get(toState.permission, false) || false);
                if (!access_token) {
                    event.preventDefault();
                    $state.go('403');

                } else {
                    if (toState.permission !== '' && hasAccess !== true || roleName === null) {
                        event.preventDefault();
                        $state.go('403');
                    }
                }
            }
        });
    };


    // Configuration function
    var _app_config = function ($stateProvider, $urlRouterProvider, $locationProvider) {


        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/home/home.template.html',
                controller: 'home-controller',
                restricted: false,
            })
            //*************************************************\\
            //=================================================\\
            //===================== EVENTS  ===================\\
            //=================================================\\
            //*************************************************\\
            .state('new-event', {
                url: '/admin/event/new',
                templateUrl: 'views/new_event/new_event.template.html',
                controller: 'new-event-controller'
            })
            .state('update-event', {
                url: '/admin/event/:event/update',
                templateUrl: 'views/new_event/new_event.template.html',
                controller: 'new-event-controller'
            })
            .state('admin.events', {
                url: '/admin/events',
                templateUrl: 'views/admin/templates/events/events.template.html',
                controller: 'events-admin-page-controller'
            })
            .state('events', {
                url: '/events',
                templateUrl: 'views/events/events.template.html',
                controller: 'events-controller'
            })
            .state('event', {
                url: '/event/:id',
                templateUrl: 'views/event/event.template.html',
                controller: 'event-controller'
            }).state('event-members', {
                url: '/event/:event/members',
                templateUrl: 'views/event_members/members.template.html',
                controller: 'member-controller'
            })

            //////////////////////////////////////////////////////////////////////
            ///////////////////////// Pages & Articles ///////////////////////////
            //////////////////////////////////////////////////////////////////////

            .state('news', {
                url: '/news',
                templateUrl: 'views/news/news.template.html',
                controller: 'news-controller',
                restricted: false,
                permission: ''
            })

            .state('new-article', {
                url: '/admin/article/new',
                templateUrl: 'views/new_article/new_article.template.html',
                controller: 'new-article-controller',
                restricted: true,
                permission: 'manage-pages'
            })
            .state('update-article', {
                url: '/admin/article/:article/update',
                templateUrl: 'views/new_article/new_article.template.html',
                controller: 'new-article-controller'
            })
            .state('new-page', {
                url: '/admin/page/new',
                templateUrl: 'views/new_page/new_page.template.html',
                controller: 'new-page-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('update-page', {
                url: '/admin/page/:page/update',
                templateUrl: 'views/new_page/new_page.template.html',
                controller: 'new-page-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('page', {
                url: '/page/:url',
                templateUrl: 'views/page/page.template.html',
                controller: 'page-controller',
                restricted: false,
                permission: ''
            })
            .state('article', {
                url: '/article/:url',
                templateUrl: 'views/article/article.template.html',
                controller: 'article-controller',
                restricted: false,
                permission: ''
            })


            //////////////////////////////////////////////////////////////////////
            //////////////////////////// Admin Page //////////////////////////////
            //////////////////////////////////////////////////////////////////////


            .state('admin', {
                url: '/admin',
                templateUrl: 'views/admin/admin.template.html',
                controller: 'admin-controller',
                restricted: true,
                permission: 'admin-access'
            })

            .state('admin.top-menu', {
                url: '/top_menu',
                templateUrl: 'views/admin/templates/top_menu/top_menu.template.html',
                controller: 'top-menu-admin-page-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('admin.pages', {
                url: '/pages',
                templateUrl: 'views/admin/templates/pages/pages.template.html',
                controller: 'pages-admin-page-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('admin.articles', {
                url: '/articles',
                templateUrl: 'views/admin/templates/articles/articles.template.html',
                controller: 'articles-admin-page-controller',
                restricted: true,
                permission: 'manage-pages'
            })


            .state('admin.options', {
                url: '/options',
                templateUrl: 'views/admin/templates/options/options.template.html',
                controller: 'options-admin-page-controller',
                restricted: true,
                permission: 'admin-access'
            })

            .state('admin.users', {
                url: '/users',
                templateUrl: 'views/admin/templates/users/users.template.html',
                controller: 'users-admin-page-controller',
                restricted: true,
                permission: 'manage-users'
            })

            .state('admin.user-roles', {
                url: '/user-roles',
                templateUrl: 'views/admin/templates/user_roles/user_roles.template.html',
                controller: 'user-roles-admin-page-controller',
                restricted: true,
                permission: 'manage-users'
            })

            .state('admin.user-points', {
                url: '/user-points',
                templateUrl: 'views/admin/templates/user_points/user_points.template.html',
                controller: 'user-points-admin-page-controller',
                restricted: true,
                permission: 'manage-users'
            })

            .state('admin.user-point-record', {
                url: '/user-points-record',
                templateUrl: 'views/admin/templates/user_point_record/user_point_record.template.html',
                controller: 'user-point-record-admin-page-controller',
                restricted: true,
                permission: 'manage-users'
            })

            .state('admin.user-point-types', {
                url: '/user-points-types',
                templateUrl: 'views/admin/templates/user_point_types/user_point_types.template.html',
                controller: 'user-point-types-admin-page-controller',
                restricted: true,
                permission: 'manage-users'
            })

            .state('admin.slides', {
                url: '/admin/slides',
                templateUrl: 'views/admin/templates/slider/slider.template.html',
                controller: 'slides-admin-page-controller',
                restricted: true,
                permission: ''
            })
            .state('new-slide', {
                url: '/slide/new',
                templateUrl: 'views/admin/templates/new_slide/new_slide.template.html',
                controller: 'new-slide-controller',
                restricted: true,
                permission: ''
            })

            .state('update-slide', {
                url: '/slide/:slide/update',
                templateUrl: 'views/admin/templates/new_slide/new_slide.template.html',
                controller: 'new-slide-controller',
                restricted: true,
                permission: ''
            })

			.state('calendar', {
				url: '/calendar',
				templateUrl: 'views/calendar/calendar.template.html',
				controller: 'calendar-controller',
				restricted: false,
				permission: ''
			})

            //////////////////////////////////////////////////////////////////////
            ///////////////////////////// User Auth //////////////////////////////
            //////////////////////////////////////////////////////////////////////
            .state('leaderboard', {
                url: '/leaderboard',
                templateUrl: 'views/leaderboard/leaderboard.template.html',
                controller: 'leaderboard-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('reset-password', {
                url: '/user/forgot/:token',
                templateUrl: 'views/reset_password/reset_password.template.html',
                controller: 'reset-password-controller',
                restricted: false,
                permission: ''
            })

            .state('validate-email', {
                url: '/user/email/validate/:token',
                templateUrl: 'views/validate_email/validate_email.template.html',
                controller: 'validate-email-controller',
                restricted: false,
                permission: ''
            })

            .state('user-forgot', {
                url: '/user/forgot',
                templateUrl: 'views/forgot_password/forgot_password.template.html',
                controller: 'forgot-password-controller',
                restricted: false,
                permission: ''
            })

            .state('user-login', {
                url: '/user/login',
                templateUrl: 'views/login/login.template.html',
                controller: 'login-controller',
                restricted: false,
                permission: ''
            })

            .state('user-login-oauth', {
                url: '/user/login/oauth',
                templateUrl: 'views/login/login.template.html',
                controller: 'login-controller',
                restricted: false,
                permission: ''
            })

            .state('user-register', {
                url: '/user/register',
                templateUrl: 'views/registration/registration.template.html',
                controller: 'registration-controller',
                restricted: false,
                permission: ''
            })

            .state('user-profile-update', {
                url: '/user/profile/:username/update',
                templateUrl: 'views/registration/registration.template.html',
                controller: 'registration-controller',
                restricted: true,
                permission: ''
            })

            .state('user-profile', {
                url: '/user/profile/:username',
                templateUrl: 'views/user_profile/user_profile.template.html',
                controller: 'user-profile-controller',
                restricted: false,
                permission: ''
            })

            .state('user-logout', {
                url: '/user/logout',
                controller: ['$scope', '$cookies', '$state', function ($scope, $cookies, $state) {
                    var cookies = $cookies.getAll();
                    angular.forEach(cookies, function (v, k) {
                        $cookies.remove(k);
                    });
                    $scope.$root.$broadcast('top-bar', null);
                    $state.go('home');
                }],
                restricted: false,
                permission: ''
            })


            //////////////////////////////////////////////////////////////////////
            ///////////////////////////// Quizzes and questions //////////////////////////////
            //////////////////////////////////////////////////////////////////////

            .state('admin.new-quiz', {
                url: '/admin/quizzes/new',
                templateUrl: 'views/new_quiz/new_quiz.template.html',
                controller: 'new-quiz-controller',
                restricted: true,
                permission: 'manage-pages'
            })
            .state('admin.quiz', {
                url: '/admin/quizzes/:id',
                templateUrl: 'views/quiz/quiz.template.html',
                controller: 'quiz-admin-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('admin.edit-quiz', {
                url: '/admin/quizzes/edit/:id',
                templateUrl: 'views/edit_quiz/edit_quiz.template.html',
                controller: 'edit-quiz-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('admin.quizzes', {
                url: '/admin/quizzes',
                templateUrl: 'views/admin/templates/quizzes/quizzes.template.html',
                controller: 'quizzes-admin-controller',
                restricted: true,
                permission: 'manage-pages'
            })

            .state('quizzes', {
                url: '/user/:username/quizzes',
                templateUrl: 'views/quizzes/quizzes.template.html',
                controller: 'quizzes-controller',
                restricted: true,
                permission: ''
            })

				.state('quizinfo', {
					//url: '/user/:username/quizzes/:id',
					url: '/quizzes/:id/user/:username',
					templateUrl: 'views/quizInfoPage/quizInfoPage.template.html',
					controller: 'quizzesInfoPage-controller',
					restricted: true,
					permission: ''
				})

            .state('admin.new-question', {
                url: '/admin/question/new/:id',
                templateUrl: 'views/new_question/new_question.template.html',
                controller: 'new-question-controller',
                restricted: true,
                permission: 'manage-pages'
            })
            .state('admin.edit-question', {
                url: '/question/edit/:id',
                templateUrl: 'views/edit_question/edit_question.template.html',
                controller: 'edit-question-controller',
                restricted: true,
                permission: 'manage-pages'
            })
            .state('admin.question', {
                url: '/admin/question/:id',
                templateUrl: 'views/question/question.template.html',
                controller: 'question-admin-controller',
                restricted: true,
                permission: 'manage-pages'
            })
            .state('take-quiz', {
                url: '/user/takequiz/:id',
                templateUrl: 'views/take_quiz/take_quiz.template.html',
                controller: 'take-quiz-controller',
                restricted: true,
                permission: ''
            })

            //////////////////////////////////////////////////////////////////////
            /////////////////////////// Error Pages //////////////////////////////
            //////////////////////////////////////////////////////////////////////

            .state('403', {
                url: '/error/403',
                templateUrl: 'errorViews/access_denied/access_denied.template.html',
                controller: 'access-denied-controller',
                restricted: false,
                permission: ''
            })

            .state('404', {
                url: '/error/404',
                templateUrl: 'errorViews/not_found/not_found.template.html',
                controller: 'not-found-controller',
                restricted: false,
                permission: ''
            })

            .state('500', {
                url: '/error/500',
                templateUrl: 'errorViews/internal_error/internal_error.template.html',
                controller: 'internal-error-controller',
                restricted: false,
                permission: ''
            });


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/error/404');
        $locationProvider.html5Mode(true);

    };

    // creates the base modules
    angular.module('EnterpriseGymApp.Factories', []);
    angular.module('EnterpriseGymApp.Directives', []);
    angular.module('EnterpriseGymApp.ErrorViews', []);
    angular.module('EnterpriseGymApp.Views', ['EnterpriseGymApp.Factories']);
    angular.module('EnterpriseGymApp.Templates', ['EnterpriseGymApp.Factories']);

    // Declares the modules
    var _app_dependencies = [
        'ngSanitize',
        'ui.router',
        'ui.tinymce',
        'ngCookies',
        'ngTable',
        'vcRecaptcha',
        'mgcrea.ngStrap.popover',
		'mgcrea.ngStrap.collapse',
        'ui.bootstrap.carousel',
		'ui.bootstrap.tabs',
		'ui.bootstrap.tpls',
        'ui.rCalendar',
		'keats.youtube',
        'EnterpriseGymApp.Factories',
        'EnterpriseGymApp.Directives',
        'EnterpriseGymApp.ErrorViews',
        'EnterpriseGymApp.Views',
        'EnterpriseGymApp.Templates'
    ];

    // Injects the Dependencies (in a way that can be compressed)
    _app_run.$inject = ['$rootScope', '$state', '$cookies'];
    _app_config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    // declares the app and runs the configuration and run methods
    angular.module('EnterpriseGymApp', _app_dependencies).run(_app_run).config(_app_config);

	angular.module('EnterpriseGymApp').controller('index-controller',['$scope', '$http', function ($scope, $http) {
		$scope.isAdminPage = (window.location.pathname.indexOf('/admin') !== -1);
		$scope.isAdminPage = ($scope.isAdminPage || (window.location.pathname.indexOf('/page/new') !== -1));
		$scope.isAdminPage = ($scope.isAdminPage || (window.location.pathname.indexOf('/event/new') !== -1));
		$scope.isAdminPage = ($scope.isAdminPage || (window.location.pathname.indexOf('/article/new') !== -1));
		$scope.isAdminPage = ($scope.isAdminPage || (window.location.pathname.indexOf('/profile') !== -1));
		$scope.isAdminPage = ($scope.isAdminPage || (window.location.pathname.indexOf('/user/takequiz') !== -1));

		$scope.$root.$on('$stateChangeStart', function (event, toState, toStateParams) {
			$scope.isAdminPage = (toState.name.indexOf('admin.') !== -1);
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('user-profile') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('new-page') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('new-event') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('new-article') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('update-page') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('update-event') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('update-article') !== -1));
			$scope.isAdminPage = ($scope.isAdminPage || (toState.name.indexOf('take-quiz') !== -1));
		});
	}]);

})();
