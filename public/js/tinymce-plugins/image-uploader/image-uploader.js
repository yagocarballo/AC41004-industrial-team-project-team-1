tinymce.PluginManager.add('imageUploader', function(editor) {
	function openImagePicker() {
		// Open window
		editor.windowManager.open({
			title: 'Upload Image',
			body: [{
				type: 'textbox',
				title: 'file',
				name: 'file',
				subtype: 'file',
				label: 'Upload'
			}],
			onsubmit: function(e) {
				// Gets the file from the filepicker
				var files = document.querySelector('input[type=file]').files;

				// Creates the form & adds the Image
				var data = new FormData();
				data.append("image", files[0]);

				// Creates the HTTP Request with credentials
				var xhr = new XMLHttpRequest();
				xhr.withCredentials = true;

				// Adds the Event listener for when the request finishes
				xhr.addEventListener("readystatechange", function() {
					if (this.readyState === this.DONE) {
						var data = JSON.parse(this.responseText);
						editor.insertContent('<img src="' + data.url + '"></img>');
					}
				}, false);

				xhr.addEventListener('progress', function(e) {
					if (e.lengthComputable) {
						var percentComplete = (e.loaded / e.total) * 100;
						// TODO: Show upload proggress
					}
				}, false);

				xhr.addEventListener('error', function() {
					alert('Error uploading image.');
				}, false);

				// Makes the API Call
				xhr.open("POST", "api/image/upload");
				xhr.send(data);
			}
		});
	}

	editor.addCommand('UploadImage', function() {
		editor.execCommand('mceInsertContent', false, '<hr />');
	});

	editor.addButton('UploadImage', {
		text: 'Upload Image',
		tooltip: 'Upload Image',
		onclick: openImagePicker
	});

	editor.addMenuItem('UploadImage', {
		text: 'Upload Image',
		onclick: openImagePicker,
		context: 'insert'
	});
});
