(
    function () {
        var _internal_error_controller = function ($scope, $http, $stateParams) {
        };

        _internal_error_controller.$inject = [ '$scope', '$http', '$stateParams' ];

        angular.module('EnterpriseGymApp.ErrorViews').controller('internal-error-controller', _internal_error_controller);
    }
)();