(
    function () {
        var _not_found_controller = function ($scope, $http, $stateParams) {
        };

        _not_found_controller.$inject = [ '$scope', '$http', '$stateParams' ];

        angular.module('EnterpriseGymApp.ErrorViews').controller('not-found-controller', _not_found_controller);
    }
)();