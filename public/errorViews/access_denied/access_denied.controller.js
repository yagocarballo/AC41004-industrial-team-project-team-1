(
    function () {
        var _access_denied_controller = function ($scope, $http, $stateParams) {
        };

        _access_denied_controller.$inject = [ '$scope', '$http', '$stateParams' ];

        angular.module('EnterpriseGymApp.ErrorViews').controller('access-denied-controller', _access_denied_controller);
    }
)();