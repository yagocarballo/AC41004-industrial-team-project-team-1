(
	function() {

		// Creates the Directive
		var _facebook_feed_directive = function($timeout) {
			return {
				restrict: 'E',
				templateUrl: 'directives/facebook_feed/facebook_feed.template.html',
				replace: true,
				link: function(scope) {
					// var finished_rendering = function() {
					//   console.log("finished rendering plugins");
					// }
					//
					// scope.loadFacebookFeed = function(d, s, id) {
					// 	var js, fjs = d.getElementsByTagName(s)[0];
					// 	if (d.getElementById(id)) d.getElementById(id).remove();
					// 	js = d.createElement(s);
					// 	js.id = id;
					// 	js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=994970067212704";
					// 	fjs.parentNode.insertBefore(js, fjs);
					//
					// 	// In your onload handler
					// 	FB.Event.subscribe('xfbml.render', finished_rendering);
					// };
					//
					// $timeout(function () {
					// 	scope.loadFacebookFeed(document, 'script', 'facebook-jssdk');
					// }, 100);

					// function checkFacebookFeed() {
					// 	if (document.getElementsByClassName("fb-page fb_iframe_widget")[0] == null) {
					// 		$timeout(function () {
					// 			scope.loadFacebookFeed(document, 'script', 'facebook-jssdk');
					// 		}, 0);
					// 	}
					// }
					// window.setInterval(checkFacebookFeed, 2000);
				}
			}
		};

		// Injects the Dependencies (in a way that can be compressed)
		_facebook_feed_directive.$inject = ['$timeout'];

		angular.module('EnterpriseGymApp.Directives').directive('facebookFeed', _facebook_feed_directive);
	}
)();
