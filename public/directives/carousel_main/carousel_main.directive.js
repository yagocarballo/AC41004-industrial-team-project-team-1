(
    function () {

        var _carousel_main_controller = function ($scope, $http) {

            $scope.slides = [];

            $http({
                url: 'api/slider/type/' + 'carousel_main',
                method: 'GET',
                dataType: 'json',
                data: null,
                headers: {}
            }).success(function (data, status, headers, config) {
                $scope.slides = data;

            }).error(function(data, status, headers, config) {
                console.error(data);
            });

            $scope.myInterval = 2000;
            $scope.noWrapSlides = false;
            var slides = $scope.slides;

            $scope.clickMe = function(clickEvent) {
                window.open(clickEvent);
            };
        };


        _carousel_main_controller.$inject = [ '$scope', '$http'];


        // Creates the Directive
        var _carousel_main_directive = function() {
            return {
                restrict: 'E',
                templateUrl: 'directives/carousel_main/carousel_main.template.html',
                replace: true,
                controller: _carousel_main_controller
            }
        };

        // Injects the Dependencies (in a way that can be compressed)
        _carousel_main_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('carouselMain', _carousel_main_directive);
    }
)();

