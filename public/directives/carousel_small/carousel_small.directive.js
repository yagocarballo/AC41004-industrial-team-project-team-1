(
    function () {

        var _carousel_small_controller = function ($scope, $http) {

            $scope.slidesSmall = [];

            $http({
                url: 'api/slider/type/' + 'carousel_small',
                method: 'GET',
                dataType: 'json',
                data: null,
                headers: {}
            }).success(function (data, status, headers, config) {
                $scope.slidesSmall = data;

            }).error(function(data, status, headers, config) {
                console.error(data);
            });

            $scope.myInterval = 2000;
            $scope.noWrapSlides = false;
            var slidesSmall = $scope.slidesSmall;

            function clearControls() {
                document.getElementsByClassName("left carousel-control")[1].outerHTML = "";
                document.getElementsByClassName("right carousel-control")[1].outerHTML = "";
            }

            setTimeout(clearControls, 50);
            $scope.clickMe = function(clickEvent) {
                window.open(clickEvent);
            };
        };


        _carousel_small_controller.$inject = [ '$scope', '$http'];


        // Creates the Directive
        var _carousel_small_directive = function() {
            return {
                restrict: 'E',
                templateUrl: 'directives/carousel_small/carousel_small.template.html',
                replace: true,
                controller: _carousel_small_controller
            }
        };

        // Injects the Dependencies (in a way that can be compressed)
        _carousel_small_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('carouselSmall', _carousel_small_directive);
    }
)();

