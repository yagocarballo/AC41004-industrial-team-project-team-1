(
	function() {
		var _hamburger_menu_controller = function($scope, $timeout, $http, $cookies, $state) {
			$scope.openProfile = function() {
				$state.go('user-profile', {
					username: $cookies.get('username', '')
				});
			};
			
			$scope.role = {};

			$scope.shouldShow = function(permission) {
				if (permission) {
					return $scope.role[permission];
				} else {
					return true;
				}
			};

			$scope.$root.$on('top-bar', function($event, newMenu) {
				if (newMenu === null) {
					$timeout($scope.checkAuth, 0);
				} else {
					$scope.menu = newMenu;
				}
			});

			$scope.fetchMenu = function() {
				$http({
					url: 'api/setting/top-bar',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function(data, status, headers, config) {
					if (data) {
						$scope.menu = JSON.parse(data.value);
					}

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			};

			$scope.checkAuth = function() {
				$scope.role = {
					'name': $cookies.get('role-name', null),
					'logged-in': ($cookies.get('access-token', null) ? true : false),
					'logged-out': ($cookies.get('access-token', null) ? false : true),
					'admin-access': JSON.parse($cookies.get('admin-access', false) || false),
					'manage-users': JSON.parse($cookies.get('manage-users', false) || false),
					'manage-pages': JSON.parse($cookies.get('manage-pages', false) || false),
					'manage-events': JSON.parse($cookies.get('manage-events', false) || false)
				};

				$scope.role['can-see-admin'] = ($scope.role['manage-users'] || $scope.role['manage-pages'] || $scope.role['manage-events']);
			};

			$timeout($scope.checkAuth, 0);
			$timeout($scope.fetchMenu, 0);
		};

		// Creates the Directive Link (pre render)
		var _hamburger_menu_link = function(scope, element, attrs) {};

		// Injects the dependencies
		_hamburger_menu_controller.$inject = ['$scope', '$timeout', '$http', '$cookies', '$state'];
		_hamburger_menu_link.$inject = [];

		// Creates the Directive
		var _hamburger_menu_directive = function() {
			return {
				require: [],
				templateUrl: 'directives/hamburger_menu/hamburger_menu.template.html',
				replace: true,
				scope: {},
				controller: _hamburger_menu_controller,
				link: _hamburger_menu_link
			}
		};

		// Injects the Dependencies (in a way that can be compressed)
		_hamburger_menu_directive.$inject = [];

		angular.module('EnterpriseGymApp.Directives').directive('hamburgerMenu', _hamburger_menu_directive);
	}
)();
