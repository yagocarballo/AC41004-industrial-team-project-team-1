(
    function () {
        var _permissions_checkbox_controller = function ($scope) {
			
        };

		// Creates the Directive Link (pre render)
		var _permissions_checkbox_link = function(scope, element, attrs) {
			element.css('position', 'relative');
			element.css('top', '5px');
			element.css('width', '34px');
			element.css('height', '24px');
			element.css('font-size', '25px');
			
			scope.icon = 'glyphicon-remove-circle';
			scope.ngModel = (scope.ngModel === true);
			scope.check = function () {
				scope.ngModel = !scope.ngModel;
			};
			
			scope.$watch('ngModel', function (newVal) {
				scope.icon = ( newVal ? 'glyphicon-ok-circle' : 'glyphicon-remove-circle' );
			});
		};

		// Injects the dependencies
        _permissions_checkbox_controller.$inject = [ '$scope' ];
		_permissions_checkbox_link.$inject = [];
		
		// Creates the Directive
		var _permissions_checkbox_directive = function() {
		    return {
		        require: [],
				template: '<span class="glyphicon" ng-class="icon" ng-click="check()" aria-hidden="true">',
		        replace: true,
		        scope: {
		        	ngModel: '='
		        },
		        controller: _permissions_checkbox_controller,
		        link: _permissions_checkbox_link
		    }
		};

		// Injects the Dependencies (in a way that can be compressed)
		_permissions_checkbox_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('permissionsCheckbox', _permissions_checkbox_directive);
    }
)();

