(
    function () {
        var _sidebar_controller = function ($scope, $http, $cookies, $state, $popover) {
			$scope.events = [];
			$scope.activePanel = -1;
			
		    $scope.changeMode = function (mode) {
		        $scope.mode = mode;
		    };

		    $scope.today = function () {
		        $scope.currentDate = new Date();
		    };

		    $scope.isToday = function () {
		        var today = new Date(),
		            currentCalendarDate = new Date($scope.currentDate);

		        today.setHours(0, 0, 0, 0);
		        currentCalendarDate.setHours(0, 0, 0, 0);
		        return today.getTime() === currentCalendarDate.getTime();
		    };

		    $scope.onEventSelected = function (event) {
		        $scope.event = event;
				$state.go('event', {
					id: event.id
				});
		    };
			
			$scope.fetchEvents = function () {
				$scope.events = [];
				$http({
					url: 'api/events',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					$scope.events = data.events;
					$scope.eventSource = data.events;
					
				}).error(function (data, status, headers, config) {
					console.error(data);
				});
			};
			
			$scope.fetchEvents();
			
			$scope.strip = function (html) {
				var tmp = document.createElement("DIV");
				tmp.innerHTML = html;
				return tmp.textContent||tmp.innerText;
			}
        };

		// Creates the Directive Link (pre render)
		var _sidebar_link = function(scope, element, attrs) {
			scope.strip = function (html) {
				var tmp = document.createElement("DIV");
				tmp.innerHTML = html;
				return tmp.textContent||tmp.innerText;
			}
		};

		// Injects the dependencies
        _sidebar_controller.$inject = [ '$scope', '$http', '$cookies', '$state','$popover' ];
		_sidebar_link.$inject = [];
		
		// Creates the Directive
		var _sidebar_directive = function() {
		    return {
		        require: [],
				templateUrl: 'directives/sidebar/sidebar.template.html',
		        replace: true,
		        scope: {
		        	ngModel: '='
		        },
		        controller: _sidebar_controller,
		        link: _sidebar_link
		    }
		};

		// Injects the Dependencies (in a way that can be compressed)
		_sidebar_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('sidebar', _sidebar_directive);
    }
)();

