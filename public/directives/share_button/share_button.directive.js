(
    function () {

        // Creates the Directive
        var _share_button_directive = function() {
            return {
                restrict: 'E',
                templateUrl: 'directives/share_button/share_button.template.html',
                replace: true,
                controller: function () {
                    current_url = window.location.href;
                    title = document.getElementsByTagName("title")[0].innerHTML;

                    share_yahoo = '<li style="float:left;margin-right: 10px;"><a href=\"http://bookmarks.yahoo.com/myresults/bookmarklet?u='+current_url+';t='+title+'\" title=\"Bookmark this post on Yahoo.\" id=\"share_yahoo\" target=\"_blank\"><i class="fa fa-yahoo fa-2x"></i></a></li>';
                    share_icio = '<li style="float:left;margin-right: 10px;"><a href=\"http://del.icio.us/post?url='+current_url+';title='+title+'\" title=\"Bookmark this post on del.icio.us.\" id=\"share_icio\" target=\"_blank\"><i class="fa fa-delicious fa-2x"></i></a></li>';
                    share_twitter = '<li style="float:left;margin-right: 10px;"><a href=\"http://twitter.com/home/?status='+current_url+'+--+'+title+'\" title=\"Share this on Twitter\" id=\"share_twitter\" target=\"_blank\"><i class="fa fa-twitter-square fa-2x"></i></a></li>';
                    share_facebook = '<li style="float:left;margin-right: 10px;"><a href=\"http://www.facebook.com/sharer.php?u='+current_url+';t='+title+'\" title=\"Share on Facebook.\" id=\"share_facebook\" target=\"_blank\"><i class="fa fa-facebook-square fa-2x"></i></a></li>';
                    share_google_bookmark = '<li style="float:left;margin-right: 10px;"><a href=\"http://www.google.com/bookmarks/mark?op=add&amp;bkmk='+current_url+';title='+title+'\" title=\"Bookmark this post on Google.\" id=\"share_google_bookmark\"target=\"_blank\"><i class="fa fa-google fa-2x"></i></a></li>';
                    share_google_buzz = '<li style="float:left;margin-right: 10px;"><a href=\"http://www.google.com/buzz/post?url='+current_url+'\" title=\"Buzz this post on Google.\" id=\"share_google_buzz\" target=\"_blank\"><i class="fa fa-google-plus-square fa-2x"></i></a></li>';
                    share_linkedin = '<li style="float:left;margin-right: 10px;"><a href=\"http://www.linkedin.com/shareArticle?mini=true&amp;url='+current_url+';title='+title+';summary=&amp;source=Enterprise+Gym\" title=\"Publish this post to LinkedIn\" id=\"share_linkedin\" target=\"_blank\"><i class="fa fa-linkedin-square fa-2x"></i></a></li>';
                    share_stumbleupon = '<li style="float:left;margin-right: 10px;"><a href=\"http://www.stumbleupon.com/submit?url='+current_url+';title='+title+'\" title=\"Thumb this up at StumbleUpon\" id=\"share_stumbleupon\" target=\"_blank\"><i class="fa fa-stumbleupon fa-2x"></i></a></li>';

                    list = document.getElementsByClassName("share-button-list")[0]
                    list.innerHTML = '<span style=\"font-size:18px;float:left;color:#23527c;margin-right:10px;\">share on</span>' +share_facebook + share_twitter +  share_yahoo + share_icio + share_google_bookmark + share_google_buzz + share_linkedin + share_stumbleupon;

                }
            }
        };

        // Injects the Dependencies (in a way that can be compressed)
        _share_button_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('shareButton', _share_button_directive);
    }
)();

