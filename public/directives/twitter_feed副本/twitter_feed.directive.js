(
	function() {

		// Creates the Directive
		var _twitter_feed_directive = function($timeout) {
			return {
				restrict: 'E',
				templateUrl: 'directives/twitter_feed/twitter_feed.template.html',
				replace: true,
				link: function(scope) {
					scope.loadTwitterFeed = function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0],
							p = /^http:/.test(d.location) ? 'http' : 'https';
						if (!d.getElementById(id)) {
							js = d.createElement(s);
							js.id = id;
							js.src = p + "://platform.twitter.com/widgets.js";
							fjs.parentNode.insertBefore(js, fjs);
						}
					};

					//setTimeout("document.getElementById('twitter-widget-0').style.width = 340 + 'px';", 2000 );

					$timeout(function(){
						scope.loadTwitterFeed(document, "script", "twitter-wjs");
					}, 200);

					function checkTwitterFeed() {
						if (document.getElementById("twitter-widget-0") == null) {
							$timeout(function(){
								scope.loadTwitterFeed(document, "script", "twitter-wjs");
							}, 0);
						}
					}

					window.setInterval(checkTwitterFeed, 2000);

				}
			}
		};

		// Injects the Dependencies (in a way that can be compressed)
		_twitter_feed_directive.$inject = ['$timeout'];

		angular.module('EnterpriseGymApp.Directives').directive('twitterFeed', _twitter_feed_directive);
	}
)();
