(
    function () {
        var _menu_builder_controller = function ($scope, $http, $cookies, $state,$popover) {
			$scope.success = false;
			$scope.pages = [];
			$scope.updateMode = null;
			$scope.newMenuItem = {
				title: '',
				url: ''
			};
			
			$scope.itemOpen = null;
			$scope.subUpdateMode = null;
			$scope.newSubMenuItem = {
				title: '',
				url: ''
			};
			
			$scope.popover = {
				templateUrl: 'popover-template.html',
				title: 'Add Menu Item'
			};
			
			$scope.systemPages = [
				{
					name: 'Home',
					url: 'home'
				},
				{
					name: 'User Profile',
					url: 'user-profile({username: \'' + $cookies.get('username', '') + '\'})'
				},
				{
					name: 'News',
					url: 'news'
				},
				{
					name: 'Admin Area',
					url: 'admin.pages'
				},
				{
					name: 'User Login',
					url: 'user-login'
				},
				{
					name: 'User Logout',
					url: 'user-logout'
				},
				{
					name: 'User Register',
					url: 'user-register'
				},
				{
					name: 'User Forgot Password',
					url: 'user-forgot'
				},
				{
					name: 'New Page',
					url: 'new-page'
				},
				{
					name: 'New Article',
					url: 'new-article'
				},
				{
					name: 'New Event',
					url: 'new-event'
				},
				{
					name: 'All events',
					url: 'events'
				},
				{
					name: 'Calendar',
					url: 'calendar'
				}
			];
			
			$scope.menu = [];
			
			$scope.modify = function ($index) {
				$scope.updateMode = $index;
				$scope.newMenuItem = $scope.menu[$index];

			};
			
			$scope.openItem = function ($index) {
				$scope.itemOpen = $index;
			};
			
			$scope.subModify = function ($index) {
				$scope.subUpdateMode = $index;
				$scope.menu[$scope.itemOpen].submenu = ($scope.menu[$scope.itemOpen].submenu || []);
				$scope.newSubMenuItem = $scope.menu[$scope.itemOpen].submenu[$index];

			};
			
			$scope.removeMenuItem = function () {
				$scope.menu.splice($scope.updateMode, 1);
			};
			
			$scope.removeSubMenuItem = function () {
				$scope.menu[$scope.itemOpen].submenu.splice($scope.subUpdateMode, 1);
			};
			
			$scope.addMenuItem = function (newMenuItem) {
				if ($scope.updateMode !== null) {
					$scope.menu[$scope.updateMode] = newMenuItem;
					document.querySelector('.popover').remove();

				} else {
					$scope.menu.push(newMenuItem);
					document.querySelector('.popover').remove();

				}
				
				$scope.newMenuItem = {
					title: '',
					url: ''
				};
			};
			
			$scope.addSubMenuItem = function (newSubMenuItem) {
				$scope.menu[$scope.itemOpen].submenu = ($scope.menu[$scope.itemOpen].submenu || []);
				
				if ($scope.subUpdateMode !== null) {
					$scope.menu[$scope.itemOpen].submenu[$scope.subUpdateMode] = newSubMenuItem;
					document.querySelector('.popover').remove();

				} else {
					$scope.menu[$scope.itemOpen].submenu.push(newSubMenuItem);
					document.querySelector('.popover').remove();
				}
				
				$scope.newSubMenuItem = {
					title: '',
					url: ''
				};
			};
			
			$scope.save = function () {
				$http({
					url: 'api/setting',
					method: 'POST',
					dataType: 'json',
					data: JSON.stringify({
						key: 'top-bar',
						value: JSON.stringify($scope.menu)
					}),
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function (data, status, headers, config) {
					$scope.$root.$broadcast('top-bar', $scope.menu);
					$scope.success= 'true';


				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			};
			
			$scope.fetchMenu = function () {
				$http({
					url: 'api/setting/top-bar',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					if (data) {
						$scope.menu = JSON.parse(data.value);
					}

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			};
			
			$scope.fetchPages = function () {
				$http({
					url: 'api/pages',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					$scope.pages = data;

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			};
			
			$scope.fetchMenu ();
			$scope.fetchPages ();
        };

		// Creates the Directive Link (pre render)
		var _menu_builder_link = function(scope, element, attrs) {
		};

		// Injects the dependencies
        _menu_builder_controller.$inject = [ '$scope', '$http', '$cookies', '$state','$popover' ];
		_menu_builder_link.$inject = [];
		
		// Creates the Directive
		var _menu_builder_directive = function() {
		    return {
		        require: [],
				templateUrl: 'directives/menu_builder/menu_builder.template.html',
		        replace: true,
		        scope: {
		        	ngModel: '='
		        },
		        controller: _menu_builder_controller,
		        link: _menu_builder_link
		    }
		};

		// Injects the Dependencies (in a way that can be compressed)
		_menu_builder_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('menuBuilder', _menu_builder_directive);
    }
)();

