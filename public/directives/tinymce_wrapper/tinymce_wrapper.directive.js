(
    function () {
        var _tinymce_wrapper_controller = function ($scope, $http, $cookies) {
			$scope.tinymceOptions = {
				inline: false,
				selector: 'textarea',
				min_height: 300,
				browser_spellcheck : true,
				plugins : [
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen emoticons",
					"insertdatetime media table contextmenu paste imageUploader"
				],
				image_list: function(success) {
					$http({
						url: 'api/images',
						method: 'GET',
						dataType: 'json',
						data: null,
						headers: {
							'Authorization' : 'Bearer ' + $cookies.accessToken
						}
					}).success(function (data, status, headers, config) {
						success(data);
				
					}).error(function(data, status, headers, config) {
						$scope.error = 'Error fetching the list of images.';
					});
			    },
				external_plugins: {
					'imageUploader': '/js/tinymce-plugins/image-uploader/image-uploader.js'
				},
				skin_url: '/css/tinymce-light',
				theme : 'modern',
				toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | image-uploader | UploadImage'
			};
        };

		// Creates the Directive Link (pre render)
		var _tinymce_wrapper_link = function(scope, element, attrs) {
		};

		// Injects the dependencies
        _tinymce_wrapper_controller.$inject = [ '$scope', '$http', '$cookies' ];
		_tinymce_wrapper_link.$inject = [];
		
		// Creates the Directive
		var _tinymce_wrapper_directive = function() {
		    return {
		        require: [],
				template: '<textarea ui-tinymce="tinymceOptions" ng-model="text"></textarea>',
		        replace: true,
		        scope: {
		        	text: '='
		        },
		        controller: _tinymce_wrapper_controller,
		        link: _tinymce_wrapper_link
		    }
		};

		// Injects the Dependencies (in a way that can be compressed)
		_tinymce_wrapper_directive.$inject = [];

        angular.module('EnterpriseGymApp.Directives').directive('tinymceWrapper', _tinymce_wrapper_directive);
    }
)();

