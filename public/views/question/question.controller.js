(
    function () {
        var _question_admin_controller = function ($scope, $http, $stateParams, $state, $window) {

			$scope.question = {
				id: 0,
				question: '',
				alternatives: [],
				answer: 0,
				quizID: 0,

			};
			$scope.questions = [];
			//get quiz
			$http({
				url: 'api/question/' + $stateParams.id,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.question.id = data.id;
				$scope.question.question = data.question;
				$scope.question.alternatives = data.alternatives.split(",");
				$scope.question.answer = parseInt(data.answer,10);
				$scope.question.quizID = data.quizID;

			}).error(function(data, status, headers, config) {
				console.error(data);
			});




			$scope.deleteQuestion = function () {

					$http({
						url: 'api/question/' + $stateParams.id,
						method: 'DELETE',
						dataType: 'json',
						data: JSON.stringify($scope.question),
						headers: {}
					}).success(function (data, status, headers, config) {
						$state.go('admin.quiz', { id: $scope.question.quizID});

					}).error(function(data, status, headers, config) {
						  alert("Quiz couldn't be deleted");
					});

			};




        };

        _question_admin_controller.$inject = [ '$scope', '$http', '$stateParams', '$state','$window' ];

        angular.module('EnterpriseGymApp.Views').controller('question-admin-controller', _question_admin_controller);
    }
)();