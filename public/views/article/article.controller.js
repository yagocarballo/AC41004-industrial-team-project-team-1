(
    function () {
        var _article_controller = function ($scope, $http, $stateParams) {
			$scope.article = {
				title: '',
				text: '',
				createdAt: ''
			};
			
			$http({
				url: 'api/article/' + $stateParams.url,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.article.title = data.title;
				$scope.article.text = data.text;
				$scope.article.createdAt = data.createdAt

			}).error(function(data, status, headers, config) {
				console.error(data);
			});
        };

        _article_controller.$inject = [ '$scope', '$http', '$stateParams' ];

        angular.module('EnterpriseGymApp.Views').controller('article-controller', _article_controller);
    }
)();