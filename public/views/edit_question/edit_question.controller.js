(
    function () {
        var _edit_question_controller = function ( $scope,$http,$state, $stateParams) {
			$scope.question = {
				id: $stateParams.id,
				question: '',
				alternatives: '',
				answer: '',
				quizID: 0
			};
			$scope.tempalt = [];

			$http({
				url: 'api/question/' + $stateParams.id,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.question.question = data.question;
				$scope.question.answer = data.answer;
				$scope.question.quizID = data.quizID;
				$scope.tempalt = data.alternatives.split(",");

			}).error(function(data, status, headers, config) {
				console.error(data);
			});


			$scope.success = false;
			$scope.error = null;

			$scope.editQuestion = function () {
				$scope.error = null;
				$scope.success = false;

				var errorMessage = checkQuestion($scope.question);
				var errorAlternatives= checkAlternatives($scope.tempalt);
				for (var i=0;i<3;i++){
					$scope.question.alternatives = $scope.question.alternatives +  $scope.tempalt[i] + ',';
				}
				$scope.question.alternatives = $scope.question.alternatives +  $scope.tempalt[3];

				if (errorMessage === null && errorAlternatives ===null) {$http({
						url: 'api/question/' + $stateParams.id,
						method: 'PUT',
						dataType: 'json',
						data: JSON.stringify($scope.question),
						headers: {}
					}).success(function (data, status, headers, config) {
						$scope.success = true;
						$state.go('admin.question', { id: $scope.question.id});
					}).error(function(data, status, headers, config) {
						$scope.error = 	'Quiz exists already.';
					});
				} else {
					$scope.error = errorMessage + errorAlternatives;
				}
			};

			function checkQuestion (question) {
				var invalid = [];


				if (!question.question || question.question === '') {
					invalid.push('question');
				}
				if (!question.answer || question.answer	 === '') {
					invalid.push('answer');
				}

				if (invalid.length !== 0) {
					return 'The fields "' + invalid.join('", "') + '" are Invalid.';
				} else {
					return null;
				}
			}
			function checkAlternatives (alt) {
				var invalid = [];


				if (!alt[0] || alt[0] === '') {
					invalid.push('Alternative a');
				}
				if (!alt[1] || alt[1] === '') {
					invalid.push('alternative b');
				}
				if (!alt[2] || alt[2]	 === '') {
					invalid.push('alternative c');
				}

				if (invalid.length !== 0) {
					return 'The fields "' + invalid.join('", "') + '" are Invalid.';
				} else {
					return null;
				}
			}

			 };

			_edit_question_controller.$inject = ['$scope','$http','$state','$stateParams'];

			angular.module('EnterpriseGymApp.Views').controller('edit-question-controller', _edit_question_controller);
		}
)();