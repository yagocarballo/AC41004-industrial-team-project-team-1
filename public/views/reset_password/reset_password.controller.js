(
    function () {
        var _reset_password_controller = function ($scope, $http, $stateParams) {
			$scope.token = $stateParams.token;
			$scope.success = false;
			$scope.error = null;
			
			$scope.resetPassword = function () {
				$scope.success = false;
				$scope.error = null;
				
				if (!$scope.new_password || $scope.new_password === '') {
					$scope.error = 'Invalid Passwords.';
				} else if ($scope.new_password !== $scope.new_password_2) {
					$scope.error = 'Passwords do not match.';
				} else {
					$http({
						url: 'api/user/forgot/' + $stateParams.token,
						method: 'POST',
						dataType: 'json',
						data: JSON.stringify({
							"new_password": CryptoJS.SHA512($scope.new_password).toString(),
							"new_password_2": CryptoJS.SHA512($scope.new_password_2).toString()
						}),
						headers: {}
					}).success(function (data, status, headers, config) {
						$scope.success = true;

					}).error(function(data, status, headers, config) {
						$scope.error = 'Error Reseting the password.';
					});
				}
			};
        };

        _reset_password_controller.$inject = [ '$scope', '$http', '$stateParams' ];

        angular.module('EnterpriseGymApp.Views').controller('reset-password-controller', _reset_password_controller);
    }
)();