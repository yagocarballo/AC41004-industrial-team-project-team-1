/**
 * Created by peterbennington on 17/09/15.
 */

(function() {
		var _login_controller = function($scope, $http, $cookies, $timeout, $state, $stateParams) {
			$scope.user = {
				username: '',
				password: ''
			};
			
			$scope.error = null;
			$scope.success = false;
			
			$scope.facebookSignIn = function () {
				window.location.href = 'api/auth/facebook';
			};
			
			$scope.signIn = function ($event) {
				$scope.error = null;
				$scope.success = false;
				
				$http({
					url: 'api/user/login',
					method: 'POST',
					dataType: 'json',
					data: JSON.stringify({
						username: $scope.user.username,
						password: CryptoJS.SHA512($scope.user.password).toString()
					}),
					headers: {}
				}).success(function (data, status, headers, config) {
					// Saves the Access-Token
					$cookies.put('access-token', data.accessToken);
					$cookies.put('username', data.username);
					if (data.roleInfo) {
						$cookies.put('role-name', data.roleInfo.name);
						$cookies.put('admin-access', data.roleInfo.adminAccess);
						$cookies.put('manage-users', data.roleInfo.manageUsers);
						$cookies.put('manage-pages', data.roleInfo.managePages);
						$cookies.put('manage-events', data.roleInfo.manageEvents);
					} else {
						$cookies.put('role-name', null);
						$cookies.put('admin-access', false);
						$cookies.put('manage-users', false);
						$cookies.put('manage-pages', false);
						$cookies.put('manage-events', false);
					}
					
					// Refreshes the Menu
					$scope.$root.$broadcast('top-bar', null);
					
					// Shows the Success Message
					$scope.success = true;
					
					// Redirects to the Home view in 1.5 seconds
					$timeout(function () {
						$state.go('user-profile', {
							username: data.username
						});
					}, 1500);

				}).error(function(data, status, headers, config) {
					if (status === 403) {
						$scope.error = 'Invalid username or password.';
					} else if (status === 404) {
						$scope.error = 'Username not found in the database.';
					} else {
						$scope.error = data.error;
					}
				});
				
				$event.preventDefault();
			};
			
			$scope.parseParams = function () {
				var params = {};
				var rawParams = window.location.search.replace('?', '').split('&');
				for (var i=0;i<rawParams.length;i++) {
					var item = rawParams[i].split('=');
					params[item[0]] = item[1];
				}
				
				return params;
			}
			
			var oauthParams = $scope.parseParams();
			if (oauthParams['access_token']) {
				// Saves the Access-Token
				$cookies.put('access-token', oauthParams['access_token']);
				$cookies.put('username', oauthParams['username']);
				
				$cookies.put('role-name', (oauthParams['role'] || null));
				$cookies.put('admin-access', (oauthParams['admin-access'] || false));
				$cookies.put('manage-users', (oauthParams['manage-users'] || false));
				$cookies.put('manage-pages', (oauthParams['manage-pages'] || false));
				$cookies.put('manage-events', (oauthParams['manage-events'] || false));
				
				// Refreshes the Menu
				$scope.$root.$broadcast('top-bar', null);
				
				// Shows the Success Message
				$scope.success = true;
				
				// Redirects to the Home view in 1.5 seconds
				$timeout(function () {
					$state.go('home');
				}, 1500);
				
			} else if (oauthParams.error) {
				if (oauthParams.error === '404') {
					$scope.error = 'User not found.';
				} else {
					$scope.error = 'Unknown error: ' + oauthParams.error;
				}
			}
		};

		_login_controller.$inject = ['$scope', '$http', '$cookies', '$timeout', '$state', '$stateParams'];
		
		angular.module('EnterpriseGymApp.Views').controller('login-controller', _login_controller);
})();
