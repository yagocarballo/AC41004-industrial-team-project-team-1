(
    function () {
        var _new_article_controller = function ($scope, $timeout, $http, $cookies, $stateParams) {
			$scope.updateMode = false;
			$scope.pages = [];

			$scope.success = false;
			$scope.error = null;
			
			$scope.article = {
				url: '',
				title: '',
				text: '',
				pageId: null,
				sticky: false
			};
			
			$scope.saveArticle = function () {
				$scope.error = null;
				$scope.success = false;
				
				var errorMessage = $scope.checkArticle($scope.article);
				if (errorMessage === null) {
					$http({
						url: (!$scope.updateMode ? 'api/article' : ('api/article/' + $stateParams.article)),
						method: ($scope.updateMode ? 'PUT' : 'POST'),
						dataType: 'json',
						data: JSON.stringify($scope.article),
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
						}
					}).success(function (data, status, headers, config) {
						$scope.success = true;

					}).error(function(data, status, headers, config) {
						$scope.error = 'Article exists already.';
					});
				} else {
					$scope.error = errorMessage;
				}
			};
			
			$scope.fetchPages = function () {
				$http({
					url: 'api/newsFeeds',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
					}
				}).success(function (data, status, headers, config) {
					$scope.pages = data;

				}).error(function(data, status, headers, config) {
					$scope.error = 'Error downloading pages list.';
				});
			};
			
			$timeout($scope.fetchPages, 0);
			
			$scope.checkArticle = function (article) {
				var invalid = [];
				
				if (!article.url || article.url === '' || article.url.indexOf(" ") !== -1) {
					invalid.push('Name');
				}
				
				if (!article.title || article.title === '') {
					invalid.push('Title');
				}
				
				if (!article.text) {
					invalid.push('Article Content');
				}
				
				if (invalid.length !== 0) {
					return 'The fields "' + invalid.join('", "') + '" are Invalid.';
				} else {
					return null;
				}
			};
			
			if ($stateParams.article) {
				$scope.updateMode = true;
				
				$http({
					url: 'api/article/' + $stateParams.article,
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
					}
				}).success(function (data, status, headers, config) {
					$scope.article = data;

				}).error(function(data, status, headers, config) {
					$scope.error = 'Error fetching the article to update.';
				});
			}
        };

        _new_article_controller.$inject = [ '$scope', '$timeout', '$http', '$cookies', '$stateParams' ];

        angular.module('EnterpriseGymApp.Views').controller('new-article-controller', _new_article_controller);
    }
)();