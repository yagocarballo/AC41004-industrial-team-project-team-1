(
    function () {
        var _admin_controller = function ($scope, $http, $state, $stateParams, $cookies) {
			$scope.role = {
				name: $cookies.get('role-name', null),
				adminAccess: JSON.parse($cookies.get('admin-access', false)),
				manageUsers: JSON.parse($cookies.get('manage-users', false)),
				managePages: JSON.parse($cookies.get('manage-pages', false)),
				manageEvents: JSON.parse($cookies.get('manage-events', false))
			};
			
			$scope.isActive = function (name) {
				return ($state.includes(name) ? 'active' : '');
			};
        };

        _admin_controller.$inject = [ '$scope', '$http', '$state', '$stateParams', '$cookies' ];

        angular.module('EnterpriseGymApp.Views').controller('admin-controller', _admin_controller);
    }
)();