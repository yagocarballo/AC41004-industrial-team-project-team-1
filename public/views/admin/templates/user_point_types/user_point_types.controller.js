(
    function () {
        var _user_point_types_admin_page_controller = function ($scope, $http, $cookies, $timeout) {
			$scope.pointTypes = [];
			$scope.editPointType = null;
			
			$scope.newPointType = {
				name: ''
			};
			
			// loads Cached User Roles
			var tmpPointTypes = localStorage.getItem('point-types');
			if (tmpPointTypes) {
				$scope.pointTypes = JSON.parse(tmpPointTypes);
			}
			
			$scope.editMode = function ($index) {
				$scope.editPointType = $index;
			};
			
			$scope.deleteUserPointType = function ($index) {
				bootbox.confirm('Are you sure you want to delete the user point type: ' + $scope.pointTypes[$index].name + '?', function (shouldDelete) {
					if (shouldDelete) {
						$http({
							url: 'api/point-type/' + $scope.pointTypes[$index].id,
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization' : 'Bearer ' + $cookies.get('access-token')
							}
						}).success(function (data, status, headers, config) {
							$scope.pointTypes.splice($index, 1);
					
						}).error(function(data, status, headers, config) {
							console.error(data);
						}).finally(function () {
							localStorage.setItem('point-types', JSON.stringify($scope.pointTypes));
						});
					}
				});
			};
			
			$scope.addUserPointType = function () {
				if ($scope.newPointType.name && $scope.newPointType.name !== '') {
					$http({
						url: 'api/point-type',
						method: 'POST',
						dataType: 'json',
						data: JSON.stringify($scope.newPointType),
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token')
						}
					}).success(function (data, status, headers, config) {
						$scope.pointTypes.push($scope.newPointType);

					}).error(function(data, status, headers, config) {
						console.error(data);
						
					}).finally(function () {
						localStorage.setItem('point-types', JSON.stringify($scope.pointTypes));
						$scope.newPointType = {
							name: ''
						};
					});
				}
			};
			
			$scope.saveUserPointType = function ($index) {
				$http({
					url: 'api/point-type/'+$scope.pointTypes[$index].id,
					method: 'PUT',
					dataType: 'json',
					data: JSON.stringify($scope.pointTypes[$index]),
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function (data, status, headers, config) {
					$scope.editPointType = null;

				}).error(function(data, status, headers, config) {
					console.error(data);
					
				}).finally(function () {
					localStorage.setItem('point-types', JSON.stringify($scope.pointTypes));
					$scope.newPointType = {
						name: ''
					};
				});
			};
			
			$scope.fetchPointTypes = function () {
				$http({
					url: 'api/point-types',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function (data, status, headers, config) {
					$scope.pointTypes = data;
					localStorage.setItem('point-types', JSON.stringify($scope.pointTypes));

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			}
			
			$timeout($scope.fetchPointTypes, 0);
        };

        _user_point_types_admin_page_controller.$inject = [ '$scope', '$http', '$cookies', '$timeout' ];

        angular.module('EnterpriseGymApp.Templates').controller('user-point-types-admin-page-controller', _user_point_types_admin_page_controller);
    }
)();