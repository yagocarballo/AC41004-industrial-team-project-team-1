(
    function () {
        var _user_roles_admin_page_controller = function ($scope, $http, $cookies, $timeout) {
			$scope.roles = [];
			$scope.editRole = null;
			
			$scope.newRole = {
				name: '',
				adminAccess: false, 
				managePages: false, 
				manageEvents: false, 
				manageUsers: false
			};
			
			// loads Cached User Roles
			var tmpUserRoles = localStorage.getItem('user-roles');
			if (tmpUserRoles) {
				$scope.roles = JSON.parse(tmpUserRoles);
			}
			
			$scope.editMode = function ($index) {
				$scope.editRole = $index;
			};
			
			$scope.permissionClass = function (isEnabled) {
				return ( isEnabled ? 'glyphicon-ok-circle' : 'glyphicon-remove-circle');
			};
			
			/**
			 * @function deleteUserRole
			 * @description Tells the API to delete the user role
			 * @param $index (integer) The position of the user role
			 */
			$scope.deleteUserRole = function ($index) {
				bootbox.confirm('Are you sure you want to delete the user role: ' + $scope.roles[$index].name + '?', function (shouldDelete) {
					if (shouldDelete) {
						$http({
							url: 'api/user-role/' + $scope.roles[$index].name,
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization' : 'Bearer ' + $cookies.get('access-token')
							}
						}).success(function (data, status, headers, config) {
							$scope.roles.splice($index, 1);
					
						}).error(function(data, status, headers, config) {
							console.error(data);
						}).finally(function () {
							localStorage.setItem('user-roles', JSON.stringify($scope.roles));
						});
					}
				});
			};
			
			/**
			 * @function addUserRole
			 * @description Tells the API to add a new user role
			 */
			$scope.addUserRole = function () {
				if ($scope.newRole.name && $scope.newRole.name !== '') {
					$http({
						url: 'api/user-role',
						method: 'POST',
						dataType: 'json',
						data: JSON.stringify($scope.newRole),
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token')
						}
					}).success(function (data, status, headers, config) {
						$scope.roles.push(data);

					}).error(function(data, status, headers, config) {
						console.error(data);
						
					}).finally(function () {
						$scope.newRole = {
							name: '',
							adminAccess: false, 
							managePages: false, 
							manageEvents: false, 
							manageUsers: false
						};
						
						localStorage.setItem('user-roles', JSON.stringify($scope.roles));
					});
				}
			};
			
			/**
			 * @function saveUserRole
			 * @description Tells the API to update the user role
			 * @param $index (integer) The position of the user role
			 */
			$scope.saveUserRole = function ($index) {
				$http({
					url: 'api/user-role/' + $scope.roles[$index].name,
					method: 'PUT',
					dataType: 'json',
					data: JSON.stringify($scope.roles[$index]),
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function (data, status, headers, config) {
					$scope.editRole = null;
					localStorage.setItem('user-roles', JSON.stringify($scope.roles));

				}).error(function(data, status, headers, config) {
					console.error(data);
					
				}).finally(function () {
					$scope.newRole = {
						name: '',
						adminAccess: false, 
						managePages: false, 
						manageEvents: false, 
						manageUsers: false
					};
					
					localStorage.setItem('user-roles', JSON.stringify($scope.roles));
				});
			};
			
			/**
			 * @function fetchUserRoles
			 * @description Fetches the User Roles from the API
			 */
			$scope.fetchUserRoles = function () {
				$http({
					url: 'api/user-roles',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function (data, status, headers, config) {
					$scope.roles = data;
					localStorage.setItem('user-roles', JSON.stringify($scope.roles));

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			}
			
			$timeout($scope.fetchUserRoles, 0);
        };

        _user_roles_admin_page_controller.$inject = [ '$scope', '$http', '$cookies', '$timeout' ];

        angular.module('EnterpriseGymApp.Templates').controller('user-roles-admin-page-controller', _user_roles_admin_page_controller);
    }
)();