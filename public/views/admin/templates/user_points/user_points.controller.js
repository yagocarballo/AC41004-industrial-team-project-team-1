(
	function() {
		var _user_points_admin_page_controller = function($scope, $timeout, $http, $cookies, $state, ngTableParams, csvExporter) {
			$scope.leaderboard = [];
			
			// Defines the table Columns
			$scope.tableCols = [
				{ field: "position", 	title: "Position", 		sortable: "position", 	show: true }, 
				{ field: "noOfPoints", 	title: "Points", 		sortable: "noOfPoints", show: true },
				{ field: "firstName", 	title: "First Name", 	sortable: "firstName", 	show: true },
				{ field: "secondName", 	title: "Second Name", 	sortable: "secondName", show: true },
				{ field: "university", 	title: "University", 	sortable: "university", show: true },
				{ field: "college", 	title: "College", 		sortable: "college", 	show: true },
				{ field: "degree",		title: "Degree", 		sortable: "degree", 	show: true },
				{ field: "year",		title: "Year", 			sortable: "year", 		show: true }
			];

			// Defines the table Settings
			$scope.tableParams = new ngTableParams({
				count: 10,
				sorting: {
					position: "desc"
				}
			}, {
				data: $scope.leaderboard
			});
			
			$scope.exportLeaderboard = function () {
				csvExporter.toCSV(angular.copy($scope.users), 'Leaderboard', true, 'Leaderboard');
			};

			// Fetches the Leaderboard from the database
			$scope.fetchLeaderboard = function () {
				$http({
					url: 'api/leaderboard',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function(data, status, headers, config) {
					$scope.leaderboard = data.users;
					for (var i = 0; i < data.users.length; i++) {
						$scope.leaderboard[i].position = i + 1;
					}

					$scope.tableParams.settings({
						data: $scope.leaderboard
					});

				}).error(function(data, status, headers, config) {
					if (status === 401) {
						$cookies.remove('access-token');
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('403');
						}, 1500);
						
					} else if (status === 403) {
						$scope.error = 'Access denied to this feature.';
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('home');
						}, 1500);
						
					} else {
						$scope.error = 'Error retrieving the leaderboard.';
					}
				});
			};
			
			// Queues the fetch Leaderboard
			$timeout($scope.fetchLeaderboard, 0);
		};

		_user_points_admin_page_controller.$inject = ['$scope', '$timeout', '$http', '$cookies', '$state', 'ngTableParams', 'csvExporter'];

		angular.module('EnterpriseGymApp.Templates').controller('user-points-admin-page-controller', _user_points_admin_page_controller);
	}
)();
