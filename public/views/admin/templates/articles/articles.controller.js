(
    function () {
        var _articles_admin_page_controller = function ($scope, $http, $cookies, ngTableParams, csvExporter) {
			$scope.stickyClass = function (isSticky) {
				return ( isSticky ? 'glyphicon-ok' : 'glyphicon-remove');
			};
			
			$scope.tableParams = new ngTableParams({
				count: 10,
		    	sorting: {
					title: "asc"
				}
		    }, { getData: getData });

			function getData($defer, params){
				$http({
					url: 'api/articles',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					$scope.articles = data;
					$defer.resolve(data);

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			}
			
			$scope.exportArticles = function () {
				csvExporter.toCSV($scope.articles.articles, 'Articles', true, 'Articles-List');
			};

			// Confirm article delete dialog
			$scope.deleteArticle = function($index, article){
				bootbox.confirm('Are you sure you want to delete the article: ' + article.title + '?', function (shouldDelete) {
					if (shouldDelete) {
						// Actual deletion of the article
						$http({
							url: 'api/article/' + article.url,
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
							}
						}).success(function (data, status, headers, config) {
							$scope.articles.articles.splice($index, 1);

						}).error(function(data, status, headers, config) {
							$scope.error = 'Unable to delete.';
						});
					}
				});
			};
        };

        _articles_admin_page_controller.$inject = [ '$scope', '$http', '$cookies', 'ngTableParams', 'csvExporter' ];

        angular.module('EnterpriseGymApp.Templates').controller('articles-admin-page-controller', _articles_admin_page_controller);
	}
)();