(
    function () {
        var _quizzes_admin_controller = function ($scope, $http) {
			$scope.quizzes = [];


			$http({
				url: 'api/quizzes',
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.quizzes = data.quizzes;

			}).error(function (data, status, headers, config) {
				console.error(data);
			});

			 };

			_quizzes_admin_controller.$inject = ['$scope', '$http'];

			angular.module('EnterpriseGymApp.Views').controller('quizzes-admin-controller', _quizzes_admin_controller);
		}
)();