(
    function () {
        var _options_admin_page_controller = function ($scope, $http, $cookies) {
			// Confirm article delete dialog
			$scope.resetUsersPoints = function() {
				bootbox.confirm('Are you sure you want to reset the points for all the users? (this operation cannot be undone)', function (shouldReset) {
					if (shouldReset) {
						// Actual deletion of the article
						$http({
							url: 'api/point-record/reset',
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
							}
						}).success(function (data, status, headers, config) {
							$scope.success = 'Point Records reseted.';

						}).error(function(data, status, headers, config) {
							$scope.error = 'Unable to reset records.';
						});
					}
				});
			};
        };

        _options_admin_page_controller.$inject = [ '$scope', '$http', '$cookies' ];

        angular.module('EnterpriseGymApp.Templates').controller('options-admin-page-controller', _options_admin_page_controller);
    }
)();