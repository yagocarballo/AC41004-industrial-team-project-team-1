(
    function () {
        var _new_slide_controller = function ($scope, $timeout, $http, $cookies, $stateParams) {
			$scope.updateMode = false;

			$scope.success = false;
			$scope.error = null;
			
			$scope.slide = {
				type: '',
				picture: '',
				url: '',
				content: ''
			};

			$scope.uploadImage = function () {
				var files = document.getElementById('slide-image-input').files;

				// Creates the form & adds the Image
				var data = new FormData();
				data.append("image", files[0]);

				// Creates the HTTP Request with credentials
				var xhr = new XMLHttpRequest();
				xhr.withCredentials = true;

				// Adds the Event listener for when the request finishes
				xhr.addEventListener("readystatechange", function() {
					if (this.readyState === this.DONE) {
						var data = JSON.parse(this.responseText);
						$scope.slide.picture = data.url;
						document.getElementsByClassName("hide-if-finished")[0].style.display = "none";
						document.getElementsByClassName("hide-if-finished")[1].style.display = "none";
						document.getElementById("image-finished").style.display = "block";
						document.getElementById("image-finished").firstElementChild.value = data.url;
					}
				}, false);

				xhr.addEventListener('progress', function(e) {
					if (e.lengthComputable) {
						var percentComplete = (e.loaded / e.total) * 100;
						// TODO: Show upload proggress
					}
				}, false);

				xhr.addEventListener('error', function() {
					alert('Error uploading image.');
				}, false);

				// Makes the API Call
				xhr.open("POST", "api/image/upload");
				xhr.send(data);
			}
			
			$scope.saveSlide = function () {
				$scope.error = null;
				$scope.success = false;
				
				var errorMessage = $scope.checkSlide($scope.slide);
				if (errorMessage === null) {
					$http({
						url: (!$scope.updateMode ? 'api/slider' : ('api/slider/' + $stateParams.slide)),
						method: ($scope.updateMode ? 'PUT' : 'POST'),
						dataType: 'json',
						data: JSON.stringify($scope.slide),
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
						}
					}).success(function (data, status, headers, config) {
						$scope.success = true;
						window.location.href="/admin/admin/slides"

					}).error(function(data, status, headers, config) {
						$scope.error = 'Slide exists already.';
					});
				} else {
					$scope.error = errorMessage;
				}
			};
			
			$scope.checkSlide = function (slide) {
				var invalid = [];


				if (!slide.type || slide.type === '') {
					invalid.push('Picture');
				}

				if (!slide.picture || slide.picture === '') {
					invalid.push('Picture');
				}

				if (!slide.url || slide.url === '') {
					invalid.push('Url');
				}

				
				if (invalid.length !== 0) {
					return 'The fields "' + invalid.join('", "') + '" are Invalid.';
				} else {
					return null;
				}
			};
			
			if ($stateParams.slide) {
				$scope.updateMode = true;
				
				$http({
					url: 'api/slider/' + $stateParams.slide,
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
					}
				}).success(function (data, status, headers, config) {
					$scope.slide = data;

				}).error(function(data, status, headers, config) {
					$scope.error = 'Error fetching the slide to update.';
				});
			}
        };

        _new_slide_controller.$inject = [ '$scope', '$timeout', '$http', '$cookies', '$stateParams' ];

        angular.module('EnterpriseGymApp.Views').controller('new-slide-controller', _new_slide_controller);
    }
)();