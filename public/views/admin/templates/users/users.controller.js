(
	/**
	 * @function users.controller.js
	 * @description This class is the controller fot the users admin page.
	 */
	function() {
		var _users_admin_page_controller = function($scope, $http, $timeout, $cookies, ngTableParams, csvExporter) {
			$scope.editUser = null;
			$scope.userRoles = [];
			$scope.pointTypes = [];
			
			// loads Cached Point Types
			var tmpPointTypes = localStorage.getItem('point-types');
			if (tmpPointTypes) {
				$scope.pointTypes = JSON.parse(tmpPointTypes);
			}
			
			// loads Cached User Roles
			var tmpUserRoles = localStorage.getItem('user-roles');
			if (tmpUserRoles) {
				$scope.userRoles = JSON.parse(tmpUserRoles);
			}
			
			// Creates the table's parameters
			$scope.tableParams = new ngTableParams({
				count: 10,
				sorting: {
					title: "asc"
				}
			}, {
				getData: $scope.fetchUsers
			});

			/**
			 * @function boolClass
			 * @description Displays the OK or Remove Icons for true / false values
			 * @param value (boolean) The bool value
			 */
			$scope.boolClass = function(value) {
				return (value ? 'glyphicon-ok' : 'glyphicon-remove');
			};

			/**
			 * @function modifyUser
			 * @description Enables the edit mode for users
			 * @param $index (integer) Sets the index of the user being edited
			 */
			$scope.modifyUser = function($index) {
				$scope.editUser = $index;
			};

			/**
			 * @function exportUsers
			 * @description Exports the user's Table into a CSV file.
			 */
			$scope.exportUsers = function() {
				csvExporter.toCSV(angular.copy($scope.users), 'Users', true, 'Users-List');
			};

			/**
			 * @function updateUserDetails
			 * @description Tells the API to Update the User details
			 * @param user (object) The User
			 */
			$scope.updateUserDetails = function(user) {
				$http({
					url: 'api/user/' + user.username,
					method: 'PUT',
					dataType: 'json',
					data: JSON.stringify(angular.copy(user)),
					headers: {
						'Authorization': 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function(data, status, headers, config) {
					$scope.editUser = null;

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			};

			/**
			 * @function fetchUsers
			 * @description Fetches the Users from the API
			 * @param $defer (promise) a promise to reply once the data is fetched
			 * @param params (object) The pagination / sorting information
			 */
			$scope.fetchUsers = function ($defer, params) {
				// TODO: Handle Pagination and Sorting
				// |- Pagination: params.count() + params.page()
				// |- Sort: params.orderBy()
				
				$http({
					url: 'api/users',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function(data, status, headers, config) {
					$scope.users = data;
					$defer.resolve(data);

				}).error(function(data, status, headers, config) {
					$defer.reject(data);
					console.error(data);
				});
			};

			/**
			 * @function fetchUserRoles
			 * @description Fetches the User Roles from the API
			 */
			$scope.fetchUserRoles = function() {
				$http({
					url: 'api/user-roles',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization': 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function(data, status, headers, config) {
					$scope.userRoles = data;
					localStorage.setItem('user-roles', JSON.stringify($scope.userRoles));

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			};

			/**
			 * @function deleteUser
			 * @description Tells the API to delete a User (after confirming it)
			 * @param $index (integer) The position of the user in the Users array
			 * @param user (object) The user
			 */
			$scope.deleteUser = function($index, user) {
				bootbox.confirm('Are you sure you want to delete the user: ' + user.username + '?', function(shouldDelete) {
					if (shouldDelete) {
						// Actual deletion of the article
						$http({
							url: 'api/user/' + user.username,
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization': 'Bearer ' + $cookies.get('access-token', null)
							}
						}).success(function(data, status, headers, config) {
							$scope.users.splice($index, 1);

						}).error(function(data, status, headers, config) {
							$scope.error = 'Unable to delete.';
						});
					}
				});
			};

			/**
			 * @function addPointsToUser
			 * @description Opens the Add Points to User Dialog
			 * @param $index (integer) The position of the user in the Users array
			 * @param user (object) The user
			 */
			$scope.addPointsToUser = function($index, user) {
				// creates the content of the dialog
				var dialogContent = '<div class="row">  ' + 
							'<div class="col-md-12"> ' + 
							'<form class="form-horizontal"> ' + 
							'<div class="form-group"> ' + 
							'<label class="col-md-4 control-label" for="name">Points</label> ' + 
							'<div class="col-md-7"> ' + 
							'<input id="points" name="points" type="number" placeholder="How many points?" value="15" class="form-control input-md"> ' + 
							'<span class="help-block">Amount of points to assign.</span> </div> ' + 
							'</div> ' + 
							'<div class="form-group"> ' + 
							'<label class="col-md-4 control-label" for="awesomeness">What type of points?</label> ' + 
							'<div class="col-md-7"> ' + 
							'<select id="point-types" name="point-types" class="form-control input-md">';
							
				// Populates the Point Type options
				for (var i=0;i<$scope.pointTypes.length;i++) {
					dialogContent += '	<option value="' + $scope.pointTypes[i].id + '">' + $scope.pointTypes[i].name + '</option>';
				}
				
				dialogContent += '</select>' + '<span class="help-block">Type of points to assign.</span> </div> ' + 
							'</div> ' + 
							'<div class="form-group"> ' + 
							'<label class="col-md-4 control-label" for="name">Points Source</label> ' + 
							'<div class="col-md-7"> ' + 
							'<input id="points-source" name="points-source" type="text" placeholder="Points Source" class="form-control input-md"> ' + 
							'<span class="help-block">Where do the points come from?</span> </div> ' + 
							'</div> ' + 
							'</div> </div>' +
							'</form> </div>  </div>';
				
				// Displays the dialog
				bootbox.dialog({
					title: "How many points you want to give?",
					message: dialogContent,
					buttons: {
						danger: {
							label: "Cancel",
							className: "btn-danger"
						},
						main: {
							label: "Add Points",
							className: "btn-success",
							callback: function() {
								var points = $('#points').val();
								var pointType = $('#point-types').val();
								var message = $('#points-source').val();
								$scope.addPoints(user.id, points, pointType, message);
							}
						}
					}
				});
			};
			
			/**
			 * @function addPoints
			 * @description Tells the API to add points to a User
			 * @param userId (integer) The User ID
			 * @param points (integer) The Amount of points to add
			 * @param pointType (integer) The points Type ID
			 * @param message (string) The Message
			 */
			$scope.addPoints = function(userId, points, pointType, message) {
				// Adds a Point Record
				$http({
					url: 'api/point-record',
					method: 'POST',
					dataType: 'json',
					data: JSON.stringify({
					    pointID : pointType,
					    noOfPoints: points,
					    pointSource: message,
					    userID : userId
					}),
					headers: {
						'Authorization': 'Bearer ' + $cookies.get('access-token', null)
					}
				}).success(function(data, status, headers, config) {
					if (data) {
						$scope.pointRecord = data;
					}

				}).error(function(data, status, headers, config) {
					$scope.error = 'Unable to list all the point types.';
				});
			};
			
			/**
			 * @function fetchPointTypes
			 * @description Fetches the point Types from the API
			 */
			$scope.fetchPointTypes = function () {
				// Fetches Point Types
				$http({
					url: 'api/point-types',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function(data, status, headers, config) {
					if (data) {
						$scope.pointTypes = data;
						localStorage.setItem('point-types', JSON.stringify($scope.pointTypes));
					}

				}).error(function(data, status, headers, config) {
					$scope.error = 'Unable to list all the point types.';
				});
			};
			
			$timeout($scope.fetchUserRoles, 0);
			$timeout($scope.fetchPointTypes, 0);
		};

		_users_admin_page_controller.$inject = ['$scope', '$http', '$timeout', '$cookies', 'ngTableParams', 'csvExporter'];

		angular.module('EnterpriseGymApp.Templates').controller('users-admin-page-controller', _users_admin_page_controller);
	}
)();
