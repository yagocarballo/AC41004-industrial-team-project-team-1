(function () {
    var _events_admin_page_controller = function ($scope, $http, ngTableParams) {
        $scope.events = [];

        $scope.tableParams = new ngTableParams({
            count: 10,
            sorting: {
                title: "asc"
            }
        }, {getData: getData});

        function getData($defer, params) {
            $http({
                url: 'api/events',
                method: 'GET',
                dataType: 'json',
                data: null,
                headers: {}
            }).success(function (data, status, headers, config) {
                $scope.events = data;
                $defer.resolve($scope.events.events);

            }).error(function (data, status, headers, config) {
                console.error(data);
            });
        }
        $scope.deleteEvent = function($index, event){
            bootbox.confirm('Are you sure you want to delete the event: ' + event.title + '?', function (shouldDelete) {
                if (shouldDelete) {
                    $http({
                        url: 'api/event/' + event.id,
                        method: 'DELETE',
                        dataType: 'json',
                        data: null
                        //headers: {
                        //    'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
                        //}
                    }).success(function (data, status, headers, config) {
                        $scope.events.events.splice($index, 1);
                    }).error(function(data, status, headers, config) {
                        $scope.error = 'Unable to delete.';
                    });
                }
            });
        };
		
		$scope.strip = function (html) {
			var tmp = document.createElement("DIV");
			tmp.innerHTML = html;
			return tmp.textContent||tmp.innerText;
		}
    };

    _events_admin_page_controller.$inject = ['$scope', '$http', 'ngTableParams'];
    angular.module('EnterpriseGymApp.Templates').controller('events-admin-page-controller', _events_admin_page_controller);
})();