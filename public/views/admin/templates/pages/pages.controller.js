(
	function() {
		var _pages_admin_page_controller = function($scope, $http, $timeout, $state, $cookies, ngTableParams, csvExporter) {
			$scope.pages = [];
			
			$scope.newsFeedClass = function (isNewsFeed) {
				return ( isNewsFeed ? 'glyphicon-ok' : 'glyphicon-remove');
			};
			
			$scope.exportPages = function () {
				csvExporter.toCSV(angular.copy($scope.pages), 'Pages', true, 'Pages-List');
			};
			
			$scope.tableParams = new ngTableParams({
				count: 10,
		    	sorting: { 
					title: "asc"
				} 
		    }, { data: $scope.pages });

			$scope.fetchPages =  function (){
				$http({
					url: 'api/pages',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					$scope.pages = data;
					$scope.tableParams.settings({
						data: $scope.pages
					});

				}).error(function(data, status, headers, config) {
					if (status === 401) {
						$cookies.remove('access-token');
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('403');
						}, 1500);
						
					} else if (status === 403) {
						$scope.error = 'Access denied to this feature.';
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('home');
						}, 1500);
						
					} else {
						$scope.error = 'Error retrieving the pages.';
					}
				});
			}
			
			$timeout($scope.fetchPages, 0);
			
			// Confirm article delete dialog
			$scope.deletePage = function($index, page) {
				bootbox.confirm('Are you sure you want to delete the page: ' + page.title + '?', function (shouldDelete) {
					if (shouldDelete) {
						// Actual deletion of the article
						$http({
							url: 'api/page/' + page.url,
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
							}
						}).success(function (data, status, headers, config) {
							$scope.pages.splice($index, 1);

						}).error(function(data, status, headers, config) {
							$scope.error = 'Unable to delete.';
						});
					}
				});
			};
		};

		_pages_admin_page_controller.$inject = ['$scope', '$http', '$timeout', '$state', '$cookies', 'ngTableParams', 'csvExporter'];

		angular.module('EnterpriseGymApp.Templates').controller('pages-admin-page-controller', _pages_admin_page_controller);
	}
)();
