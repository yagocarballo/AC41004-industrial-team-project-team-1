(
    function () {
        var _slides_admin_page_controller = function ($scope, $http, $cookies, ngTableParams) {
			$scope.slides = [];
			$scope.deleteSuccess = 0;
			
			$scope.tableParams = new ngTableParams({
				count: 10,
		    }, { getData: getData });

			function getData($defer, params){
				$http({
					url: 'api/slider',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					$scope.slides = data;
					$defer.resolve($scope.slides.slides);

				}).error(function(data, status, headers, config) {
					console.error(data);
				});
			}


			// Confirm slide delete dialog
			$scope.deleteSlide = function($index, slide){
				bootbox.confirm('Are you sure you want to delete the slide?', function (shouldDelete) {
					if (shouldDelete) {
						// Actual deletion of the slide
						$http({
							url: 'api/slider/' + slide.id,
							method: 'DELETE',
							dataType: 'json',
							data: null,
							headers: {
								'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
							}
						}).success(function (data, status, headers, config) {
							document.querySelectorAll("tr.ng-scope")[$index].style.display = "none";
							$scope.slides.slides.splice($index, 1);

						}).error(function(data, status, headers, config) {
							$scope.error = 'Unable to delete.';
						});
					}
				});
			};

        };

        _slides_admin_page_controller.$inject = [ '$scope', '$http', '$cookies', 'ngTableParams' ];

        angular.module('EnterpriseGymApp.Templates').controller('slides-admin-page-controller', _slides_admin_page_controller);
	}
)();