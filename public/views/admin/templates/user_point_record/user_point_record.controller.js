(
	function() {
		var _user_point_record_admin_page_controller = function($scope, $timeout, $http, $filter, $cookies, $state, ngTableParams, csvExporter) {
			$scope.pointsRecord = [];
			$scope.fromDate = null;
			$scope.toDate = null;
			$scope.username = null;
			
			$scope.activePanel = -1;
			
			$scope.clearDates = function () {
				$scope.fromDate = null;
				$scope.toDate = null;
				$scope.username = null;
				$scope.fetchPointsRecord();
			};
			
			$scope.filterResult = function () {
				$scope.fetchPointsRecord();
			};
			
			$scope.exportPointRecord = function () {
				csvExporter.toCSV(angular.copy($scope.pointsRecord), 'Points Record', true, 'Points-Record-List');
			};
			
			// Defines the table Columns
			$scope.tableCols = [
				{ field: "id", 				title: "Id", 				sortable: "id", 	show: true },
				{ field: "noOfPoints", 		title: "Points", 			sortable: "noOfPoints", 	show: true },
				{ field: "pointSource", 	title: "Points Source", 	sortable: "pointSource", 	show: true },
				{ field: "pointType", 		title: "Points Type", 		sortable: "pointType", 		show: true },
				{ field: "createdAt", 		title: "Points Date", 		sortable: "createdAt", 		show: true },
				{ field: "username", 		title: "Username", 			sortable: "username", 		show: true },
				{ field: "email",			title: "Email", 			sortable: "email", 			show: true },
				{ field: "matriculation", 	title: "Matric Number", 	sortable: "matriculation", 	show: true }
			];
			
			// Defines the table Settings
			$scope.tableParams = new ngTableParams({
				count: 10,
				sorting: {
					position: "desc"
				}
			}, {
				data: $scope.pointsRecord
			});
			
			// Fetches the Leaderboard from the database
			$scope.fetchPointsRecord = function () {
				var url = 'api/point-records';
				
				if ($scope.username) {
					url += '?username='+$scope.username
				}
				
				if ($scope.fromDate && $scope.toDate) {
					url += ($scope.username ? '&' : '?');
					url += 'from='+ $filter('date')($scope.fromDate, "dd-MM-yyyy");
					url += '&to=' + $filter('date')($scope.toDate, "dd-MM-yyyy");
				}
				
				$http({
					url: url,
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function(data, status, headers, config) {
					$scope.pointsRecord = data;
					$scope.tableParams.settings({
						data: $scope.pointsRecord
					});

				}).error(function(data, status, headers, config) {
					if (status === 401) {
						$cookies.remove('access-token');
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('403');
						}, 1500);
						
					} else if (status === 403) {
						$scope.error = 'Access denied to this feature.';
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('home');
						}, 1500);
						
					} else {
						$scope.error = 'Error retrieving the points record.';
					}
				});
			};
			
			// Queues the fetch Leaderboard
			$timeout($scope.fetchPointsRecord, 0);
		};

		_user_point_record_admin_page_controller.$inject = ['$scope', '$timeout', '$http', '$filter', '$cookies', '$state', 'ngTableParams', 'csvExporter'];

		angular.module('EnterpriseGymApp.Templates').controller('user-point-record-admin-page-controller', _user_point_record_admin_page_controller);
	}
)();
