/**
 * Created by andrew on 20/09/15.
 */
(function(){
    var _new_event_controller = function($scope,$http,$stateParams){
        $scope.tinymceOptions = {
            inline: false,
            selector: 'textarea',
            min_height: 300,
            browser_spellcheck : true,
            plugins : [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            skin_url: '/css/tinymce-light',
            theme : 'modern',
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        };

        $scope.success = false;
        $scope.error = null;

        $scope.event = {
            id:'',
            title:'',
            description: '',
            eventType: '',
            startTime:'',
            endTime:'',
            workshopHosts:''
        };
        $http({
           url : '/api/point-types',
            method : 'GET',
            data : null
        }).success(function(data){
            $scope.eventTypes = data;
        }).error(function (error){
            console.log(error);
        });

        if($stateParams.event){
            $scope.updateMode = true;
            $http({
                url : 'api/event/' + $stateParams.event,
                method: 'GET',
                dataType: 'json',
                data : null
            }).success(function (data){
                $scope.event = data;
                $scope.event.startTime = new Date(data.startTime);
                $scope.event.endTime = new Date(data.endTime);
            }).error(function (error){
                $scope.error = 'Error fetching event to update';
            })
        }

        $scope.createEvent = function(){
            $scope.error = null;
            $scope.sucess = false;
            $scope.event.pointId = $scope.event.eventType.id;
            $scope.event.eventType = $scope.event.eventType.name;
            //var errorMessage = checkEvent($scope.event);
            var errorMessage = null;
            if(errorMessage === null) {
                $http({
                    url : (!$scope.updateMode ? 'api/event' : ('api/event/' +$stateParams.event)),
                    method : ($scope.updateMode? 'PUT' : 'POST'),
                    dataType : 'json',
                    data: JSON.stringify($scope.event),
                    headers:{}
                }).success(function(data,header,status,config){
                    $scope.success = true;
                }).error(function(data,status,headers,config){
                    $scope.error = status;
                })
            }

        };

        };
    _new_event_controller.$inject =  ['$scope', '$http' ,'$stateParams'];
    angular.module('EnterpriseGymApp.Views').controller('new-event-controller', _new_event_controller);

})();