(
    function () {
        var _news_controller = function ($scope, $http, $stateParams, $state, $window) {
			$scope.title = 'Articles';
			$scope.articles = [];
			//$scope.isAdminPage = (window.location.pathname.indexOf('/admin') !== -1);


			$http({
				url: 'api/articles',
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.articles = data;


			}).error(function(data, status, headers, config) {
				console.error(data);
			});
        };

        _news_controller.$inject = [ '$scope', '$http', '$stateParams', '$state','$window' ];

        angular.module('EnterpriseGymApp.Views').controller('news-controller', _news_controller);
    }
)();