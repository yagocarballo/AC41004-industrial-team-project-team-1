/**
 * Created by andrew on 17/09/15.
 */
(function () {
    var _events_controller = function ($scope, $http) {
        $scope.title = 'Events';
        $scope.events = [];
        //alert(events);
        $http({
            url: 'api/events',
            method: 'GET',
            dataType: 'json',
            data: null,
            headers: {}
        }).success(function (data, status, headers, config) {
            $scope.title = data.title;
            $scope.events = data.events;
        }).error(function (data, status, headers, config) {
            console.error(data);
        });
    };
    _events_controller.$inject = [ '$scope', '$http' ];
    angular.module('EnterpriseGymApp.Views').controller('events-controller', _events_controller)
})();