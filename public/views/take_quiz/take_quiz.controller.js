(

    function() {
        var _take_quiz_controller = function ($scope, $http, $timeout, $stateParams, $cookies) {
            $scope.quiz = {
                id: 0,
                title: '',
                passrate: 0


            };
            $scope.questions = [];

            $scope.id = 0;
            $scope.score = 0;
            $scope.quizOver = false;
            $scope.inProgress = true;
            $scope.questionStatus = true;
            $scope.fileExists = false;
            $scope.take_quizComplete = "";

            $scope.result = {
                username: $cookies.get('username'),
                quizID: parseInt($stateParams.id),
                result: 0,
                status: ''

            };
            $scope.points = {
                userID: '',
                pointSource:''
                };

            $http({
                url: 'api/quiz/' + $stateParams.id,
                method: 'GET',
                dataType: 'json',
                data: null,
                headers: {}
            }).success(function (data, status, headers, config) {
                $scope.quiz.id = data.id;
                $scope.quiz.title = data.title;
                $scope.points.pointSource= data.title;
                $scope.quiz.passrate = data.passrate;


            }).error(function(data, status, headers, config) {
                console.error(data);
            });
            $http({
                url: 'api/user/' + $cookies.get('username'),
                method: 'GET',
                dataType: 'json',
                data: null,
                headers: {
                    'Authorization' : 'Bearer ' + $cookies.get('access-token')
                }
            }).success(function (data, status, headers, config) {
                $scope.points.userID= data.id;
            }).error(function(data, status, headers, config) {
                console.error(data);
            });

            $scope.loadQuestions = function () {
                $http({
                    url: 'api/questions/' + $stateParams.id,
                    method: 'GET',
                    dataType: 'json',
                    data: null,
                    headers: null
                }).success(function (data, status, headers, config) {
                    $scope.questions = data.questions;
                    $scope.getQuestion();

                }).error(function (data, status, headers, config) {
                    console.log("The quiz doesn't exist");
                });
            };
            $scope.loadQuestions();

            //Gets the question from the json file and sends it to the view
            $scope.getQuestion = function () {
                var question = $scope.readQuestion($scope.id);
               // console.log(question);
                $timeout(function () {
                    if (question) {
                        $scope.question = question.question;
                        $scope.options = question.alternatives.split(',');
                        $scope.answer = question.answer;
                        $scope.answerMode = true;
                        $scope.questionStatus = true;
                    } else {
                        if ($scope.score == 0) {
                            if (!$scope.id > 0) {
                                $scope.fileExists = true;
                            }
                        }
                        $scope.result.result = ($scope.score/$scope.id) *100;
                        $scope.quizOver = true;
                        if ($scope.result.result >= $scope.quiz.passrate){
                            $scope.result.status = true;
                        }else{
                            $scope.result.status= false;
                        }
                        $scope.sendResult();

                    }
                    $scope.correctAns = false;
                    $scope.wrongAns = false;
                }, 1200);
            };



            //When pressing a option it will check if the answer is right or wrong, if right gets next question
            $scope.checkAnswer = function (ans) {
                if (ans.option) return;

                if (ans == $scope.options[$scope.answer]) {
                    $scope.score++;
                    $scope.correctAns = true;
                    $scope.wrongAns = false;
                } else {
                    $scope.wrongAns = true;
                    $scope.correctAns = false;
                }
                $scope.questionStatus = false;
                $scope.id++;
                $scope.getQuestion();

            };

            $scope.readQuestion = function (id) {
                //console.log($scope.questions.length);
                if ($scope.questions != null && id < $scope.questions.length) {
                    return $scope.questions[id];
                }
                return false;
            };
            $scope.sendResult = function() {
                $http({
                    url: 'api/quizResult',
                    method: 'POST',
                    dataType: 'json',
                    data: JSON.stringify($scope.result),
                    headers: {
                        'Authorization' : 'Bearer ' + $cookies.get('access-token')
                    }
                    }).success(function (data, status, headers, config) {
                    if ($scope.result.status) {
                        $scope.addPoints();
                    }

                }).error(function (data, status, headers, config) {
                    $scope.error = 'Quiz exists already.';
                });
            };
            $scope.addPoints = function(){
                $http({
                    url: 'api/quizpoint-record',
                    method: 'POST',
                    dataType: 'json',
                    data: JSON.stringify($scope.points),
                    headers: {
                        'Authorization' : 'Bearer ' + $cookies.get('access-token')
                    }
                }).success(function (data, status, headers, config) {
                    $scope.success = true;


                }).error(function (data, status, headers, config) {
                    $scope.error = 'Quiz exists already.';
                });

            }


        $scope.backing = function(){
            window.history.go(-2);
        };
        }
        _take_quiz_controller.$inject = ['$scope', '$http', '$timeout', '$stateParams', '$cookies'];
        angular.module('EnterpriseGymApp.Views').controller('take-quiz-controller', _take_quiz_controller);
    }
)();
