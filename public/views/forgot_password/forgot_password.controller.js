(function() {
		var _forgot_controller = function($scope, $http, $cookies) {
			$scope.email = '';
			$scope.error = null;
			$scope.success = false;
			
			$scope.forgot = function ($event) {
				$event.preventDefault();
				
				$scope.error = null;
				$scope.success = false;
				
				if ($scope.email === '' || $scope.email.indexOf('@') === -1) {
					$scope.error = 'Invalid Email.';
				} else {
					$http({
						url: 'api/user/forgot',
						method: 'POST',
						dataType: 'json',
						data: JSON.stringify({
							email: $scope.email
						}),
						headers: {}
					}).success(function (data, status, headers, config) {
						$scope.success = true;

					}).error(function(data, status, headers, config) {
						$scope.error = data.error;
					});
				}
			};
		};

		_forgot_controller.$inject = ['$scope', '$http', '$cookies'];
		
		angular.module('EnterpriseGymApp.Views').controller('forgot-password-controller', _forgot_controller);
})();
