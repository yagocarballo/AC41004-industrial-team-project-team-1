(
    function () {
        var _new_page_controller = function ($scope, $http, $cookies, $timeout, $state, $stateParams) {
			$scope.updateMode = false;
			
			// Resets the error and success messages
			$scope.success = false;
			$scope.error = null;
			
			$scope.page = {};	
			$scope.pageType = '0';
			
			/**
			 * @function createPage
			 * @description Creates the page in the Backend.
			 */
			$scope.savePage = function () {
				// Resets the error and success messages
				$scope.error = null;
				$scope.success = false;
				
				// Checks whether the page attributes are valid
				var errorMessage = $scope.checkPage($scope.page);
				if (errorMessage === null) {
					$scope.page.newsFeed = ($scope.pageType === '1');
					$scope.page.hasQuizes = ($scope.pageType === '2');
					
					// Calls the Create Page Endpoint
					$http({
						url: (!$scope.updateMode ? 'api/page' : ('api/page/' + $stateParams.page)),
						method: ($scope.updateMode ? 'PUT' : 'POST'),
						dataType: 'json',
						data: JSON.stringify($scope.page),
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token')
						}
					}).success(function (data, status, headers, config) {
						$scope.success = true;

					}).error(function(data, status, headers, config) {
						if (status === 401) {
							$cookies.remove('access-token');
							
							// Redirects to the Home view in 1.5 seconds
							$timeout(function () {
								$state.go('403');
							}, 1500);
							
						} else if (status === 403) {
							$scope.error = 'Access denied to this feature.';
							
							// Redirects to the Home view in 1.5 seconds
							$timeout(function () {
								$state.go('home');
							}, 1500);
							
						} else {
							$scope.error = 'Page exists already.';
						}
					});
				} else {
					$scope.error = errorMessage;
				}
			};
			
			/**
			 * @function checkPage
			 * @description Checks if the fields of the page are valid.
			 * @param page (Object): The page object to be checked.
			 * @return (String): Error message (null if page is valid).
			 */
			$scope.checkPage = function (page) {
				var invalid = [];
				
				// Checks that the ID is Valid
				if (!page.url || page.url === '' || page.url.indexOf(" ") !== -1) {
					invalid.push('Name');
				}
				
				// Checks that the title is Valid
				if (!page.title || page.title === '') {
					invalid.push('Title');
				}
				
				if (invalid.length !== 0) {
					return 'The fields "' + invalid.join('", "') + '" are Invalid.';
				} else {
					return null;
				}
			};
			
			if ($stateParams.page) {
				$scope.updateMode = true;
				
				$http({
					url: 'api/page/' + $stateParams.page,
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token', null)
					}
				}).success(function (data, status, headers, config) {
					$scope.page = data;
					if (data.newsFeed === true) {
						$scope.pageType = '1';
					} else if (data.hasQuizes === true) {
						$scope.pageType = '2';
					}

				}).error(function(data, status, headers, config) {
					$scope.error = 'Error fetching the page to update.';
				});
			}
        };

        _new_page_controller.$inject = [ '$scope', '$http', '$cookies', '$timeout', '$state', '$stateParams' ];

        angular.module('EnterpriseGymApp.Views').controller('new-page-controller', _new_page_controller);
    }
)();