(
    function () {
        var _validate_email_controller = function ($scope, $http, $timeout, $state, $stateParams) {
			$scope.token = $stateParams.token;
			$scope.success = false;
			$scope.error = null;
			
			$http({
				url: 'api/user/email/validate/' + $stateParams.token,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.success = true;

			}).error(function(data, status, headers, config) {
				$scope.error = 'Error Validating the email.';
			}).finally(function () {
				$timeout(function () {
					$state.go('home');
				}, 1500);
			});
        };

        _validate_email_controller.$inject = [ '$scope', '$http', '$timeout', '$state', '$stateParams' ];

        angular.module('EnterpriseGymApp.Views').controller('validate-email-controller', _validate_email_controller);
    }
)();