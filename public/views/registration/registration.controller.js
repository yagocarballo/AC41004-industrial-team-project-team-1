/**
 * Created by peterbennington on 17/09/15.
 */

(
	function() {
		var _registration_controller = function($scope, $http, $timeout, $cookies, $state, $stateParams, vcRecaptchaService) {
			$scope.recapchaKey = '6LfsQg0TAAAAACKDJFvMo4CvXHrzROQUwf3M1wIO';
			$scope.updateMode = false;
			
			$scope.notAMachine = false;
			$scope.success = false;
			$scope.error = null;

			$scope.users = [];

			$scope.emailSource = '@dundee.ac.uk';
			$scope.password = '';
			$scope.newUser = {
				username: '',
				email: '',
				universityEmail: '',
				password: '',
				firstName: '',
				secondName: '',
				country: '',
				university: '',
				status: '',
				college: '',
				degree: '',
				year: '',
				matriculation: ' ',
				terms: false,
				password: null,
				memberOfSociety: false
			};

			$scope.register = function($event) {
				$event.preventDefault();
				
				$scope.error = null;
				$scope.success = false;

				var errorMessage = $scope.checkUsername($scope.newUser);
				if (errorMessage === null) {
					if ($scope.updateMode && !$scope.password && $scope.password === '') {
						delete $scope.newUser.password;
					} else if (!$scope.password && $scope.password === '') {
						$scope.error = 'Invalid Password';
						return;
					} else {
						$scope.newUser.password = CryptoJS.SHA512($scope.password).toString();
					}
					
					$http({
						url: (!$scope.updateMode ? 'api/user' : ('api/user/' + $stateParams.username)),
						method: ($scope.updateMode ? 'PUT' : 'POST'),
						dataType: 'json',
						data: JSON.stringify($scope.newUser),
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token')
						}

					}).success(function(data, status, config) {
						$scope.success = true;
						
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('home', {
								username: $scope.newUser.username
							});
						}, 1500);

					}).error(function(data, status, config) {
						$scope.error = data.error.errors[0].message;
						// $scope.error = 'Invalid data or Username already exists.';
					});

				} else {
					$scope.error = errorMessage;
				}
			};

			$scope.checkUsername = function(newUser) {
				var error = null;
				
				// Validates University Email
				if (newUser.universityEmail.indexOf($scope.emailSource) === -1) {
					error = 'Invalid university email';
				}
				
				return error;
			}
			
			$scope.setResponse = function (response) {
				$http({
					url: 'api/recapcha/validate/' + response,
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}

				}).success(function(data, status, config) {
					if (data.success) {
						$timeout(function () {
							$scope.notAMachine = true;
						}, 0);
					}

				}).error(function(data, status, config) {
					$scope.error = data.message;
				});
			};

			$scope.cbExpiration = function() {
				$timeout(function () {
					$scope.notAMachine = false;
				}, 0);
			};
			
			if ($stateParams.username) {
				$scope.updateMode = true;
				
				$http({
					url: 'api/user/' + $stateParams.username,
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {
						'Authorization' : 'Bearer ' + $cookies.get('access-token')
					}
				}).success(function (data, status, headers, config) {
					if (data) {
						$scope.newUser = data;
					} else {
						$scope.updateMode = false;
					}

				}).error(function(data, status, headers, config) {
					if (status === 401) {
						$cookies.remove('access-token');
					
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('403');
						}, 1500);
					
					} else if (status === 403) {
						$scope.error = 'Access denied to this feature.';
					
						// Redirects to the Home view in 1.5 seconds
						$timeout(function () {
							$state.go('home');
						}, 1500);
					
					} else {
						$scope.error = 'Error retrieving the points record.';
					}
				});
			}

			$scope.universities =
				[
					"None",
					"University of Dundee",
					"University of Abertay",
					"Dundee College",
					"University of St. Andrews"
				];

			$scope.statuses =
				[
					"Undergraduate",
					"Postgraduate",
					"Graduate",
					"Alumni",
					"Staff",
					"Event",
					"Organiser",
					"Other",
					"None"
				];

			$scope.schools = {
				"University of Dundee" :
					[
						"Art & Design",
						"Dentistry",
						"Education & Social Work"
					]/*,
				"University of Abertay" :
					[
						"abertay course1",
						"abertay course2"
					],
				"Dundee College" :
					[
						"dc course1",
						"dc course2"
					]*/
			};

			$scope.degrees = {
				"Art & Design" :
					[
						"Animation BDes",
						"Art, Philosophy, Contemporary Practices BA",
						"Art & Design (General Foundation) BA / BDes"
					],
				"Dentistry" :
					[
						"Dentistry BDS",
						"Oral Health Sciences BSc",
						"Predental Year Entry BDS"
					],
				"Education & Social Work" :
					[
						"Childhood Practice / Childhood Studies BA (BACP) / BA (BACS)",
						"Community Learning and Development BA",
						"Education MA (Hons)"
					]
			};
		}

		_registration_controller.$inject = ['$scope', '$http', '$timeout', '$cookies', '$state', '$stateParams', 'vcRecaptchaService'];
		angular.module('EnterpriseGymApp.Views').controller('registration-controller', _registration_controller);
	}
)();
