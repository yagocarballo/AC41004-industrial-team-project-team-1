(function () {
    var _member_controller = function ($scope, $http, $stateParams , $cookies) {

        $scope.givePointsForEvent = function (userId , confirmed){
            $http({
                url: (confirmed? '/api/point-record' : '/api/point-record/user/'+userId+'/pointSource/'+$scope.event.title),
                method : (confirmed? 'POST' : 'DELETE'),
                dataType : 'json',
                data : {
                    pointID : $scope.event.pointId,
                    noOfPoints : $scope.event.pointsPerEvent,
                    pointSource : $scope.event.title,
                    userID : userId

                },
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('access-token', null)
                }
            }).success(function(){
                $scope.success = true;
            }).error(function(error){
                console.log(error);
                $scope.error = error;
            });
        };
        $scope.userAttends = function (confirmed, userId) {
            $http({
                url: 'api/event/' + $scope.event.id + '/member/' + userId + '/confirm',
                method: 'PUT',
                dataType: 'json',
                data: {
                    confirmed: confirmed
                }
            }).success(function () {
                $scope.success = true;
                $scope.givePointsForEvent(userId , confirmed);
            }).error(function (error) {
                $scope.error = error;
            });

        };

        $scope.event = {
            title: '',
            eventType: '',
            pointsPerEvent: 0,
            date: '',
            workshopHosts: ''
        };
        $http({
            url: 'api/event/' + $stateParams.event,
            method: 'GET',
            dataType: 'json',
            data: null,
            headers: {}
        }).success(function (data, status, header) {
            $scope.event.id = data.id;
            $scope.event.title = data.title;
            $scope.event.eventType = data.eventType;
            $scope.event.pointsPerEvent = data.pointsPerEvent;
            $scope.event.description = data.description;
            $scope.event.startTime = data.startTime;
            $scope.event.endTime = data.endTime;
            $scope.event.workshopHosts = data.workshopHosts;
            $scope.eventAttendance = data.EventAttendances;
            $scope.event.pointId = data.pointId;
            $scope.userName = $cookies.get('username');
            $scope.isLoggedIn = ($scope.userName || false);
        }).error(function (data, status, header, config) {
            console.error(data);
        });




    };
        _member_controller.$inject = ['$scope', '$http', '$stateParams', '$cookies'];
        angular.module('EnterpriseGymApp.Views').controller('member-controller', _member_controller)
})();
