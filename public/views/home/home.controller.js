(
    function () {
        var _home_controller = function ($scope, $http) {
			$scope.title = 'Articles';
			$scope.articles = [];
			//$scope.isAdminPage = (window.location.pathname.indexOf('/admin') !== -1);


			$http({
				url: 'api/articles',
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.articles = data;


			}).error(function(data, status, headers, config) {
				console.error(data);
			});
			
			$scope.strip = function (html) {
				var tmp = document.createElement("DIV");
				tmp.innerHTML = html;
				return tmp.textContent||tmp.innerText;
			}
        };

        _home_controller.$inject = [ '$scope', '$http' ];

        angular.module('EnterpriseGymApp.Views').controller('home-controller', _home_controller);
    }
)();