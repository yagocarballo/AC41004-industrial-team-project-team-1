(
    function () {
        var _leaderboard_controller = function ($scope, $http) {
            $scope.title = 'Leaderboard';
            $scope.leaderboard = [];
            $scope.sortType     = 'position'; // set the default sort type
            $scope.sortReverse  = false;  // set the default sort order

            $http({
                url: 'api/leaderboard',
                method: 'GET',
                dataType: 'json',
                data: null,
                headers: {}
            }).success(function (data, status, headers, config) {
                $scope.title = data.title;
                $scope.leaderboard = data.users;
                for (var i=0;i<data.users.length;i++)
                {
                    $scope.leaderboard[i].position = i+1;
                }

            }).error(function(data, status, headers, config) {
                console.error(data);
            });
        };

        _leaderboard_controller.$inject = [ '$scope', '$http' ];

        angular.module('EnterpriseGymApp.Views').controller('leaderboard-controller', _leaderboard_controller);
    }
)();