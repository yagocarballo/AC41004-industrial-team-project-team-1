(
    function () {
        var _user_profile_controller = function ($scope, $http, $timeout, $state, $stateParams, $cookies) {
			$scope.assistedEvents = [];
			$scope.silverStarProggress = 0;
			$scope.goldenStarProggress = 0;
			$scope.gravatarURL = 'http://www.gravatar.com/avatar/';
			$scope.user = {
				username: '',
				email: null,
				pointSummary: [
					{ points: 0 },
					{ points: 0 }
				]
			};
			
			// Loads a default gravatar
			$scope.avatar = $scope.gravatarURL + CryptoJS.MD5($scope.user.email).toString() + '?s=120&d=mm';
			
			// loads Cached User Profile
			var tmpUserProfile = localStorage.getItem('user-profile');
			if (tmpUserProfile) {
				$scope.user = JSON.parse(tmpUserProfile);
				$scope.avatar = $scope.user.avatar;
				$scope.silverStarProggress = $scope.user.silverStarProggress;
				$scope.goldenStarProggress = $scope.user.goldenStarProggress;
			}
			
			// loads Cached User Profile
			var tmpUserProfileEvents = localStorage.getItem('user-profile-events');
			if (tmpUserProfileEvents) {
				$scope.assistedEvents = JSON.parse(tmpUserProfileEvents);
			}
			
			$http({
				url: 'api/user/' + $stateParams.username + '/profile',
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {
					'Authorization' : 'Bearer ' + $cookies.get('access-token')
				}
			}).success(function (data, status, headers, config) {
				if (data) {
					$scope.user = data;
					$scope.avatar = $scope.gravatarURL + CryptoJS.MD5($scope.user.email).toString() + '?s=120&d=mm';
					
					$scope.silverStarProggress = Math.round(($scope.user.pointSummary[0].points / 70) * 100);
					$scope.goldenStarProggress = Math.round((($scope.silverStarProggress >= 100 ? 100 : $scope.silverStarProggress) + (($scope.user.pointSummary[1].points / 70) * 100)) * 0.5);
					$scope.silverStarProggress = (($scope.silverStarProggress >= 100) ? 100 : $scope.silverStarProggress) - $scope.goldenStarProggress;
					
					
					$scope.user.avatar = $scope.avatar;
					$scope.user.silverStarProggress = $scope.silverStarProggress;
					$scope.user.goldenStarProggress = $scope.goldenStarProggress;
					localStorage.setItem('user-profile', JSON.stringify($scope.user));
					
				} else {
					$state.go('404');
				}
			}).error(function(data, status, headers, config) {
				if (status === 401) {
					$cookies.remove('access-token');
					
					// Redirects to the Home view in 1.5 seconds
					$timeout(function () {
						$state.go('403');
					}, 1500);
					
				} else if (status === 403) {
					$scope.error = 'Access denied to this feature.';
					
					// Redirects to the Home view in 1.5 seconds
					$timeout(function () {
						$state.go('home');
					}, 1500);
					
				} else {
					$scope.error = 'Error retrieving the points record.';
				}
			});
			
			
			$http({
				url: 'api/events/' + $stateParams.username + '/assisted',
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {
					'Authorization' : 'Bearer ' + $cookies.get('access-token')
				}
			}).success(function (data, status, headers, config) {
				if (data) {
					$scope.assistedEvents = data;
					localStorage.setItem('user-profile-events', JSON.stringify($scope.assistedEvents));
				}

			}).error(function(data, status, headers, config) {
				if (status === 401) {
					$cookies.remove('access-token');
					
					// Redirects to the Home view in 1.5 seconds
					$timeout(function () {
						$state.go('403');
					}, 1500);
					
				} else if (status === 403) {
					$scope.error = 'Access denied to this feature.';
					
					// Redirects to the Home view in 1.5 seconds
					$timeout(function () {
						$state.go('home');
					}, 1500);
					
				} else {
					$scope.error = 'Error retrieving the events for this user.';
				}
			});
        };

        _user_profile_controller.$inject = [ '$scope', '$http', '$timeout', '$state', '$stateParams', '$cookies' ];

        angular.module('EnterpriseGymApp.Views').controller('user-profile-controller', _user_profile_controller);
    }
)();