(
    function () {
        var _new_quiz_controller = function ( $scope,$http,$state) {
			$scope.quiz = {
				title: '',
				description: '',
				resources: '',
				passrate: 80
			};
			$scope.tempres = [];

			$scope.success = false;
			$scope.error = null;


			$scope.deleteItem = function (index) {
				$scope.tempres.splice(index, 1);
			};
			$scope.uploadFile = function () {


				// Gets the file from the filepicker
				var files = document.getElementById("uploadFile").files;

				// Creates the form & adds the Image
				var data = new FormData();
				data.append("image", files[0]);

				// Creates the HTTP Request with credentials
				var xhr = new XMLHttpRequest();
				xhr.withCredentials = true;

				// Adds the Event listener for when the request finishes
				xhr.addEventListener("readystatechange", function () {
					if (this.readyState === this.DONE) {
						var data = JSON.parse(this.responseText);
						//$scope.tempres[$scope.tempres.length] = data.url;
						$scope.$apply(function(){
							$scope.tempres.push(data.url);
						});

						alert("upload complete");

					}
				}, false);


				xhr.addEventListener('error', function () {
					alert('Error uploading image.');
				}, false);

				// Makes the API Call
				xhr.open("POST", "api/image/upload");
				xhr.send(data);


			}

			$scope.createQuiz = function () {
				$scope.error = null;
				$scope.success = false;

				for (var i = 0; i < $scope.tempres.length - 1; i++) {
					$scope.quiz.resources = $scope.quiz.resources + $scope.tempres[i] + ',';
				}
				if ($scope.tempres.length != 0) {
					$scope.quiz.resources = $scope.quiz.resources + $scope.tempres[$scope.tempres.length - 1];
				}

				var errorMessage = checkQuiz($scope.quiz);
				if (errorMessage === null) {
					$http({
						url: 'api/quiz',
						method: 'POST',
						dataType: 'json',
						data: JSON.stringify($scope.quiz),
						headers: {}
					}).success(function (data, status, headers, config) {
						$scope.success = true;
						$state.go('admin.quizzes');

					}).error(function (data, status, headers, config) {
						$scope.error = 'Quiz exists already.';
					});
				} else {
					$scope.error = errorMessage;
				}
			};
			function checkQuiz(quiz) {
				var invalid = [];


				if (!quiz.title || quiz.title === '') {
					invalid.push('Title');
				}
				if (!quiz.description || quiz.description === '') {
					invalid.push('Title');
				}

				if (invalid.length !== 0) {
					return 'The fields "' + invalid.join('", "') + '" are Invalid.';
				} else {
					return null;
				}
			}
		};
		_new_quiz_controller.$inject = ['$scope','$http','$state'];

			angular.module('EnterpriseGymApp.Views').controller('new-quiz-controller', _new_quiz_controller);
		}
)();