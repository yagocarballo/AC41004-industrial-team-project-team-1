(
    function () {
        var _quizzes_controller = function ($scope, $http, $stateParams, $cookies) {


			$http({
				url: 'api/quizzes',
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.quizzes = data.quizzes;

			}).error(function (data, status, headers, config) {
				console.error(data);
			});

			$http({
				url: 'api/quizzes/' + $stateParams.username,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {
					'Authorization' : 'Bearer ' + $cookies.get('access-token')
				}
			}).success(function (data, status, headers, config) {
				$scope.quizzesTaken = data.quizzesTaken;

			}).error(function (data, status, headers, config) {
				console.error(data);
				$scope.error = 'Sorry, you are not authorized to access someone else\'s quizzes.';
			});
		};

			_quizzes_controller.$inject = ['$scope', '$http', '$stateParams', '$cookies'];

			angular.module('EnterpriseGymApp.Views').controller('quizzes-controller', _quizzes_controller);
		}
)();