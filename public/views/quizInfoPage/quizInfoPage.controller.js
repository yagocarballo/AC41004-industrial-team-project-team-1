(
    function () {
        var _quizInfoPage_controller = function ($scope, $http, $stateParams, $state, $window, $cookies) {
			$scope.quiz = {
				title: '',
				numberOfQuestions: 0,
				attemptsAllowed: 1,
				available: '',
				passrate: 0,
				description: '',
				resource: [],
				id:0
			};

			//get quiz info
			$http({
				url: 'api/quiz/' + $stateParams.id + '/user/' + $stateParams.username,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {
					'Authorization' : 'Bearer ' + $cookies.get('access-token')
				}
			}).success(function (data, status, headers, config) {
				$scope.quiz.title = data.title;
				$scope.quiz.numberOfQuestions = data.count;
				$scope.quiz.available = data.status;
				$scope.quiz.passrate = data.passrate;
				$scope.quiz.description = data.description;
				$scope.quiz.id=data.id;
				$scope.quiz.resource = data.resource.split(",");
				if ($scope.quiz.resource[0].length ==0){
					$scope.quiz.resource = [];
				}

			}).error(function(data, status, headers, config) {
				console.error(data);
				$scope.error = 'Sorry, you are not authorized to access this data.';
			});

        };

        _quizInfoPage_controller.$inject = [ '$scope', '$http', '$stateParams', '$state','$window', '$cookies' ];

        angular.module('EnterpriseGymApp.Views').controller('quizzesInfoPage-controller', _quizInfoPage_controller);
    }
)();