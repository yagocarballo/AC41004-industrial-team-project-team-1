(
    function () {
        var _quiz_admin_controller = function ($scope, $http, $stateParams, $state, $window) {
			$scope.quiz = {
				id: 0,
				title: '',
				description: '',
				passrate: 0,
				resource: [],


			};
			$scope.question = {
				id: 0,
				question: '',
				alternatives: [],
				answer: '',
				quizid: 0,

			};
			$scope.questions = [];
			//get quiz
			$http({
				url: 'api/quiz/' + $stateParams.id,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.quiz.id = data.id;
				$scope.quiz.title = data.title;
				$scope.quiz.description = data.description;
				$scope.quiz.passrate = data.passrate;
				$scope.quiz.resource = data.resource.split(",");
				if ($scope.quiz.resource[0].length ==0){
					$scope.quiz.resource = [];
				}


			}).error(function(data, status, headers, config) {
				console.error(data);
			});



			// get questions
			$http({
				url: 'api/questions/' + $stateParams.id,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.questions = data.questions;

			}).error(function(data, status, headers, config) {
				console.error(data);
			});


			$scope.deleteQuiz = function () {

				for(var i=0;i<$scope.questions.length;i++){
					$http({
						url: 'api/question/' + $scope.questions[i].id,
						method: 'DELETE',
						dataType: 'json',
						data: JSON.stringify($scope.questions[i]),
						headers: {}
					}).success(function (data, status, headers, config) {
					}).error(function(data, status, headers, config) {
						alert("Quiz couldn't be deleted");
					});
				};

					$http({
						url: 'api/quiz/+' + $stateParams.id,
						method: 'DELETE',
						dataType: 'json',
						data: JSON.stringify($scope.quiz),
						headers: {}
					}).success(function (data, status, headers, config) {
						$state.go('admin.quizzes');

					}).error(function(data, status, headers, config) {
						  alert("Quiz couldn't be deleted");
					});
			};



        };

        _quiz_admin_controller.$inject = [ '$scope', '$http', '$stateParams', '$state','$window' ];

        angular.module('EnterpriseGymApp.Views').controller('quiz-admin-controller', _quiz_admin_controller);
    }
)();