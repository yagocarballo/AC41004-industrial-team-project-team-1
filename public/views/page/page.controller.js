(
    function () {
        var _page_controller = function ($scope, $http, $stateParams, $cookies) {
			$scope.username = $cookies.get('username', '');
			$scope.articles = [];
			$scope.page = {
				title: '',
				text: '',
				description: ''
			};
			$scope.quizzes = [];
			$scope.quizzesTaken = [];

			$http({
				url: 'api/page/' + $stateParams.url,
				method: 'GET',
				dataType: 'json',
				data: null,
				headers: {}
			}).success(function (data, status, headers, config) {
				$scope.page = data;
				$scope.refreshNewsFeed();
				$scope.refreshQuizes();

			}).error(function(data, status, headers, config) {
				console.error(data);
			});


			$scope.refreshNewsFeed = function () {
				if ($scope.page.newsFeed) {
					$http({
						url: '/api/page/'  + $scope.page.id + '/articles',
						method: 'GET',
						dataType: 'json',
						data: null,
						headers: {}
					}).success(function (data, status, headers, config) {
						$scope.articles = data;

					}).error(function(data, status, headers, config) {
						console.error(data);
					});
				}
			};

			
			$scope.refreshQuizes = function () {
				if ($scope.page.hasQuizes) {
					$http({
						url: 'api/quizzes',
						method: 'GET',
						dataType: 'json',
						data: null,
						headers: {}
					}).success(function (data, status, headers, config) {
						$scope.quizzes = data.quizzes;

					}).error(function (data, status, headers, config) {
						console.error(data);
					});


					$http({
						url: 'api/quizzes/' + $cookies.get('username'),
						method: 'GET',
						dataType: 'json',
						data: null,
						headers: {
							'Authorization' : 'Bearer ' + $cookies.get('access-token')
						}
					}).success(function (data, status, headers, config) {
						$scope.quizzesTaken = data.quizzesTaken;
					}).error(function (data, status, headers, config) {
						console.error(data);
						$scope.error = 'Sorry, you are not authorized to access someone else\'s quizzes.';
					});
					$scope.sucess = function(ind){
						if($scope.quizzesTaken[ind].result >=$scope.quizzesTaken[ind].passrate){
							return "list-group-item-success"
						}
						else{
							return "list-group-item-danger"
						}
					};
				}
			};

        };

        _page_controller.$inject = [ '$scope', '$http', '$stateParams','$cookies'];

        angular.module('EnterpriseGymApp.Views').controller('page-controller', _page_controller);
    }
)();