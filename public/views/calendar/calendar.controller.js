(
    function () {
        var _calendar_controller = function ($scope, $http, $stateParams, $state, $window) {
		    $scope.changeMode = function (mode) {
		        $scope.mode = mode;
		    };

		    $scope.today = function () {
		        $scope.currentDate = new Date();
		    };

		    $scope.isToday = function () {
		        var today = new Date(),
		            currentCalendarDate = new Date($scope.currentDate);

		        today.setHours(0, 0, 0, 0);
		        currentCalendarDate.setHours(0, 0, 0, 0);
		        return today.getTime() === currentCalendarDate.getTime();
		    };

		    $scope.loadEvents = function () {
		        $scope.eventSource = createRandomEvents();
		    };

		    $scope.onEventSelected = function (event) {
		        $scope.event = event;
				$state.go('event', {
					id: event.id
				});
		    };
			
			$scope.fetchEvents = function () {
				$scope.events = [];
				$http({
					url: 'api/events',
					method: 'GET',
					dataType: 'json',
					data: null,
					headers: {}
				}).success(function (data, status, headers, config) {
					$scope.eventSource = data.events;
					
				}).error(function (data, status, headers, config) {
					console.error(data);
				});
			};
        };

        _calendar_controller.$inject = [ '$scope', '$http', '$stateParams', '$state','$window' ];

        angular.module('EnterpriseGymApp.Views').controller('calendar-controller', _calendar_controller);
    }
)();