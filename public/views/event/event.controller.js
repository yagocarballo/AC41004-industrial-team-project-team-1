/**
 * Created by andrew on 18/09/15.
 */
(function () {
    var _event_controller = function ($scope, $http, $stateParams, $cookies) {
        $scope.isLoggedIn = $cookies.get('username', null);
        $scope.title = '';
        $scope.event = {
            title: '',
            eventType: '',
            pointsPerEvent: 0,
            date: '',
            workshopHosts: ''
        };
        $http({
            url: 'api/event/' + $stateParams.id,
            method: 'GET',
            dataType: 'json',
            data: null,
            headers: {}
        }).success(function (data, status, header) {
            $scope.event.id = data.id;
            $scope.event.title = data.title;
            $scope.event.eventType = data.eventType;
            $scope.event.pointsPerEvent = data.pointsPerEvent;
            $scope.event.description = data.description;
            $scope.event.startTime = data.startTime;
            $scope.event.endTime = data.endTime;
            $scope.event.workshopHosts = data.workshopHosts;
            $scope.eventAttendance = data.EventAttendances;
            $scope.userName = $cookies.get('username');
            $scope.isLoggedIn = ($scope.userName || false);
            console.log("username is : " + $scope.userName);
        }).error(function (data, status, header, config) {
            console.error(data);
        });


        $scope.saveEvent = function () {
            $http({
                url: 'api/user/' + $scope.userName + '/profile',
                method: 'GET',
                dataType: 'json'
            }).success(function (data) {
                $scope.userId = data.id;

                $scope.error = null;
                $scope.success = false;
                $http({
                    url: 'api/event/' + $stateParams.id + '/register/' + $scope.userName,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        attendeeName: $scope.userName,
                        eventId: $stateParams.id,
                        attendee: $scope.userId
                    },
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('access-token', null)
                    }
                }).success(function (data, header, status, config) {
                    $scope.success = true;
                }).error(function (data, status, headers, config) {
                    $scope.error = data.message;
                })
            }).error(function (data) {
                console.error(data);
            });

        }
    };

    _event_controller.$inject = ['$scope','$http','$stateParams','$cookies'];
    angular.module('EnterpriseGymApp.Views').controller('event-controller',_event_controller)

})();
