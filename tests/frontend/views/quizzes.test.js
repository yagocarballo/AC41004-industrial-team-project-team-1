describe('Unit Test: Quizzes-admin View Controller', function(){
	var scope, controller;
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('quizzes-admin-controller', {
			$scope: scope
		});
	}));

    it('- Article is defined and has title and text.', function() {
		expect(scope.quizzes).toBeDefined();
		expect(scope.quizzes).toBe([]);
    });
});