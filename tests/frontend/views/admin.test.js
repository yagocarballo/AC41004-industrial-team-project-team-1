describe('Unit Test: Article View Controller', function(){
	var scope, controller;
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('admin-controller', {
			$scope: scope
		});
	}));
});