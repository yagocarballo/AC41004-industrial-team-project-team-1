describe('Unit Test: New Page View Controller', function(){
	var scope, controller;
	
	var article = {
		id: '',
		title: '',
		text: '',
		pageId: null,
		sticky: false
	};
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('new-article-controller', {
			$scope: scope
		});
	}));
	
    it('- TinyMCE Options are defined.', function() {
		expect(scope.tinymceOptions).toBeDefined();
    });

    it('- Article is defined', function() {
		expect(scope.article).toBeDefined();
		expect(JSON.stringify(scope.article)).toBe(JSON.stringify(article));
    });
	
    it('- createArticle function is defined and works', function() {
		expect(scope.createArticle).toBeDefined();
		
		var error = scope.checkArticle(article);
		expect(error).toBe('The fields "Name", "Title", "Article Content" are Invalid.');
		
		article.id = 'The-Name';
		error = scope.checkArticle(article);
		expect(error).toBe('The fields "Title", "Article Content" are Invalid.');
		
		article.title = 'The-Title';
		error = scope.checkArticle(article);
		expect(error).toBe('The fields "Article Content" are Invalid.');
		
		article.text = '-TheText-';
		error = scope.checkArticle(article);
		expect(error).toBeNull();
    });
});