describe('Unit Test: Article View Controller', function(){
	var scope, controller;
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('reset-password-controller', {
			$scope: scope
		});
	}));

    it('- resetPassword function to be defined.', function() {
		expect(scope.resetPassword).toBeDefined();
    });
});