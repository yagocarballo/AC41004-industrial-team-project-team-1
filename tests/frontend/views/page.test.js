describe('Unit Test: Page View Controller', function(){
	var scope, controller;
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('page-controller', {
			$scope: scope
		});
	}));

    it('- Page is defined and has title, text and description.', function() {
		expect(scope.page).toBeDefined();
		expect(scope.page.title).toBe('');
		expect(scope.page.text).toBe('');
		expect(scope.page.description).toBe('');
    });
	
    it('- Articles are defined', function() {
		expect(scope.articles).toBeDefined();
		expect(scope.articles.length).toBe(0);
    });
});