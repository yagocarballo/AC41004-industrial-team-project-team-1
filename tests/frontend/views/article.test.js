describe('Unit Test: Article View Controller', function(){
	var scope, controller;
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('article-controller', {
			$scope: scope
		});
	}));

    it('- Article is defined and has title and text.', function() {
		expect(scope.article).toBeDefined();
		expect(scope.article.title).toBe('');
		expect(scope.article.text).toBe('');
    });
});