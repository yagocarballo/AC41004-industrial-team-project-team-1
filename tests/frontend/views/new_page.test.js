describe('Unit Test: New Page View Controller', function(){
	var scope, controller;
	
	var page = {
		id: '',
		title: '',
		description: '',
		text: '',
		newsFeed: false
	};
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('new-page-controller', {
			$scope: scope
		});
	}));
	
    it('- TinyMCE Options are defined.', function() {
		expect(scope.tinymceOptions).toBeDefined();
    });

    it('- Page is defined', function() {
		expect(scope.page).toBeDefined();
		expect(JSON.stringify(scope.page)).toBe(JSON.stringify(page));
    });
	
    it('- createPage function is defined and works', function() {
		expect(scope.createPage).toBeDefined();
		
		var error = scope.checkPage(page);
		expect(error).toBe('The fields "Name", "Title" are Invalid.');
		
		page.id = 'The-Name';
		error = scope.checkPage(page);
		expect(error).toBe('The fields "Title" are Invalid.');
		
		page.title = '-TheTitle-';
		error = scope.checkPage(page);
		expect(error).toBeNull();
    });
});