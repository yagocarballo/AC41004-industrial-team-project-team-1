describe('Unit Test: Home View Controller', function(){
	var scope, controller;
	
	beforeEach(angular.mock.module('EnterpriseGymApp'));
	beforeEach(angular.mock.module('EnterpriseGymApp.Views'));
	
	beforeEach(angular.mock.inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('home-controller', {
			$scope: scope
		});
	}));

    it('- Title and Articles are defined and empty.', function() {
		expect(scope.title).toBe('');
		expect(scope.articles.length).toBe(0);
    });
});