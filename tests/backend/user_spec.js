var request = require('request');
var server = 'http://127.0.0.1:3000';

describe('Unit Test: API User Endpoint', function() {
	// Set the headers
	var headers = {
	    'Content-Type': 'application/json'
	};
	
    it('- Should create a new user.', function(done) {
    	// Configure the request
    	var options = {
    		url: server + '/api/user',
    		method: 'POST',
    		headers: headers,
    		body: JSON.stringify({
    			firstName: 'Unit',
    			secondName: 'Test',
    			country: 'UK',
    			university: 'Dundee',
    			status: 'Registered',
    			college: 'Computing',
    			degree: 'Applied Computing',
    			year: '2015',
    			matriculation: '123123123',
    			memberOfSociety: true,
    			username: 'unitTest-User',
    			password: 'invalid_password',
    			email: 'test@example.com',
    			mobileNumber: '0 645 7722 901',
    			contactNumber: null
    		})
    	}

    	// Start the request
    	request(options, function(error, response, body) {
    		expect(response.statusCode).toBe(200);

    		if (!error && response.statusCode == 200) {
				var user = JSON.parse(body);
    			expect(user.username).toEqual('unitTest-User');
    			done();
    		} else {
    			done(error);
    		}
    	});
    });
	
    it('- Should create a new user.', function(done) {
    	// Configure the request
    	var options = {
    		url: server + '/api/user/login',
    		method: 'POST',
    		headers: headers,
    		body: JSON.stringify({
    			username: 'unitTest-User',
    			password: 'invalid_password'
    		})
    	}

    	// Start the request
    	request(options, function(error, response, body) {
    		expect(response.statusCode).toBe(200);

    		if (!error && response.statusCode == 200) {
				var user = JSON.parse(body);
    			expect(user.accessToken).not.toBe(null);
				headers['Authorization'] = 'Bearer ' + user.accessToken;
    			done();
    		} else {
    			done(error);
    		}
    	});
    });
	
    it('- Should get user "unitTest-User".', function(done) {
    	// Configure the request
    	var options = {
    		url: server + '/api/user/unitTest-User',
    		method: 'GET',
    		headers: headers,
    		body: null
    	}

    	// Start the request
    	request(options, function(error, response, body) {
    		expect(response.statusCode).toBe(200);

    		if (!error && response.statusCode == 200) {
				var user = JSON.parse(body);
    			expect(user.firstName).toEqual('Unit');
    			done();
    		} else {
    			done(error);
    		}
    	});
    });
	
    it('- Should update user "unitTest-User".', function(done) {
    	// Configure the request
    	var options = {
    		url: server + '/api/user/unitTest-User',
    		method: 'PUT',
    		headers: headers,
    		body: JSON.stringify({
    			firstName: 'Updated',
    			password: 'password'
    		})
    	}

    	// Start the request
    	request(options, function(error, response, body) {
    		expect(response.statusCode).toBe(200);

    		if (!error && response.statusCode == 200) {
				var user = JSON.parse(body);
    			expect(user.firstName).toEqual('Updated');
    			done();
    		} else {
    			done(error);
    		}
    	});
    });
	
    it('- Should get user "unitTest-User".', function(done) {
    	// Configure the request
    	var options = {
    		url: server + '/api/user/unitTest-User',
    		method: 'GET',
    		headers: headers,
    		body: null
    	}

    	// Start the request
    	request(options, function(error, response, body) {
    		expect(response.statusCode).toBe(200);

    		if (!error && response.statusCode == 200) {
				var user = JSON.parse(body);
    			expect(user.firstName).toEqual('Updated');
    			done();
    		} else {
    			done(error);
    		}
    	});
    });
	
    it('- Should remove the user "unitTest-User".', function(done) {
    	// Configure the request
    	var options = {
    		url: server + '/api/user/unitTest-User',
    		method: 'DELETE',
    		headers: headers,
    		body: null
    	}

    	// Start the request
    	request(options, function(error, response, body) {
    		expect(response.statusCode).toBe(200);
			done();
    	});
    });
});
