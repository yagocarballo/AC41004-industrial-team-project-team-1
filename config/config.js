var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'manual-express'
    },
    port: (process.env.PORT || 3000),
    db: 'sqlite://localhost/manual-express-development.sqlite',
    storage: rootPath + '/data/manual-express-development.sqlite',
    facebook: {
  	  id: '910041105728322',
  	  secret: 'f1ad582b5b7f06efa69eeb138aae8421',
  	  callback: "http://enterprise-gym.yagocarballo.me/api/auth/facebook/callback"
    },
  },

  test: {
    root: rootPath,
    app: {
      name: 'manual-express'
    },
    port: 3000,
    db: 'sqlite://localhost/manual-express-test.sqlite',
    storage: rootPath + '/data/manual-express-test.sqlite'
  },

  production: {
    root: rootPath,
    app: {
      name: 'manual-express'
    },
    port: (process.env.PORT || 3000),
    db: 'sqlite://localhost/manual-express-production.sqlite',
    storage: rootPath + 'data/manual-express-production.sqlite'
  }
};

module.exports = config[env];
