var express = require('express');
var glob = require('glob');

var serveStatic = require('serve-static')

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var passport = require('passport');

passport.serializeUser(function(user, done) {
	var safeUser = user.dataValues;
	delete safeUser.password;
	done(null, safeUser);
});

passport.deserializeUser(function(user, done) {
	var safeUser = user.dataValues;
	delete safeUser.password;
	done(null, safeUser);
});

passport.transformAuthInfo(function(info, done) {
	info.scope.getUserRole().then(function (role) {
		if (role) {
			info.role = role;
			done(null, info);
		} else {
			done({ message: 'User does not have enough permissions to use this feature.' });
		}
	});
});

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';
  
  app.set('views', config.root + '/app/views');
  app.set('view engine', 'jade');

  // app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(serveStatic(config.root + '/public', {'index': ['index.html']}));
  // app.use(express.static(config.root + '/public'));
  app.use(passport.initialize());
  app.use(methodOverride());

  var APIcontrollers = glob.sync(config.root + '/app/controllers/api/*.js');
  APIcontrollers.forEach(function (apiController) {
    require(apiController)(app);
  });

  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });
  
  app.all("/*", function(req, res, next) {
    res.sendFile("index.html", { root: config.root + '/public' });
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  
  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });

};
