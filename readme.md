### Enterprise Gym
#### AC41004 - Industrial Team Project

##### Structure

```bash

/app ## folder with the `Backend` Code
	/app/controllers ## folder with the backend `endpoint` files
	/app/models ## folder with the `database models`
	/app/views ## folder with the error pages templates (can be ignored)
	
/config ## folder with the `configuration` files

/data ## folder with the `SQLite` database

/public ## folder with the `frontend` files
	/public/index.html ## main `frontend` file
	/public/css ## folder with the global `css` files
	/public/img ## folder with the `images`
	/public/js ## folder with global `Javascript` files
		/public/js/app.angular.js ## Main Angular javascript file
	/public/views ## folder with angular views
		/public/views/{ view_name }/{ view_name }.controller.js ## View's Javascript File
		/public/views/{ view_name }/{ view_name }.style.css ## View's CSS File
		/public/views/{ view_name }/{ view_name }.template.html ## View's HTML File

/app.js ## Node.js javascript file
/bower.json ## frontend info & dependencies
/package.json ## node.js info & dependencies
/Gruntfile.js ## build script (to automate stuff)

```

##### Getting Started

**Requirements:**
- `Node.js` installed in the computer
- `Bower.js` installed in the computer

**To Install Dependencies run this on a Terminal / Windows CMD:**

```bash

$ node install
$ bower install

```

**To run the Server run this on a Terminal / Windows CMD:**

```bash

$ node app ## This will start the server at `http://127.0.0.1:3000/`

```
